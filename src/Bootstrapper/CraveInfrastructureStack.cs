using Amazon.CDK;
using Bootstrapper.Stacks.ApiGateway;
using Bootstrapper.Stacks.Cognito;
using Bootstrapper.Stacks.Route53;
using Constructs;
using Crave.Infrastructure.Enums;
using Crave.Infrastructure.Models;

namespace Bootstrapper
{
    public class CraveInfrastructureStackProps : StackProps
    {
        public CraveEnvironment Environment { get; set; }
    }

    public class CraveInfrastructureStack : Stack
    {
        internal CraveInfrastructureStack(Construct scope, string id, CraveInfrastructureStackProps props) : base(scope, id, props)
        {
            _ = new CraveCloudGatewayPublic(this, new CraveCloudGatewayPublic.CraveCloudGatewayPublicProps
            {
                CraveEnv = props.Environment
            });

            _ = new CraveCognito(this, new CraveCognito.CraveCognitoProps
            {
                CraveEnv = props.Environment
            });

            _ = new CraveXRaySamplingRules(this, new CraveXRaySamplingRules.CraveXRaySamplingRulesProps
            {
                CraveEnv = props.Environment
            });
        }
    }
}
