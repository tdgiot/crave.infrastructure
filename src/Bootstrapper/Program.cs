﻿using System;
using Amazon.CDK;
using Bootstrapper.Stacks.ElastiCache;
using Bootstrapper.Stacks.IAM;
using Bootstrapper.Stacks.RDS;
using Bootstrapper.Stacks.Route53;
using Crave.Infrastructure.CDK.Extensions;
using Crave.Infrastructure;
using Crave.Infrastructure.Enums;
using Crave.Infrastructure.Models;
using Environment = Amazon.CDK.Environment;

namespace Bootstrapper
{
    sealed class Program
    {
        public static void Main(string[] args)
        {
            App app = new App();

            CraveEnvironment craveEnvironment = app.GetCraveEnvironment();

            Console.WriteLine("Deploy Environment: {0}", craveEnvironment.Environment);
            Console.WriteLine("Account: {0}, Region: {1}", craveEnvironment.AccountId, craveEnvironment.Region);

            EnvironmentInfrastructure(app, craveEnvironment);

            app.Synth();
        }

        private static void EnvironmentInfrastructure(App app, CraveEnvironment craveEnvironment)
        {
            Environment deployEnvironment = craveEnvironment.ToCdkEnvironment();
            
            CraveInfrastructureStack baseInfra = new CraveInfrastructureStack(app, "crave-infra-base",
                new CraveInfrastructureStackProps
                {
                    Environment = craveEnvironment,
                    Env = deployEnvironment
                });

            CraveManagedRoles managedRoles = new CraveManagedRoles(app, "crave-infra-managed-roles", new StackProps
            {
                Env = deployEnvironment
            });
            managedRoles.AddDependency(baseInfra);

            MSSQLDatabaseStack mainDatabaseStack = new MSSQLDatabaseStack(app, "crave-infra-database",
                new MSSQLDatabaseStack.MSSQLDatabaseProps
                {
                    Env = deployEnvironment,
                    CraveEnv = craveEnvironment
                });
            mainDatabaseStack.AddDependency(baseInfra);

            RedisCacheStack redisCacheStack = new RedisCacheStack(app, "crave-infra-redis",
                new RedisCacheStack.RedisCacheStackProps
                {
                    Env = deployEnvironment,
                    CraveEnv = craveEnvironment
                });
            redisCacheStack.AddDependency(baseInfra);
        }
    }
}