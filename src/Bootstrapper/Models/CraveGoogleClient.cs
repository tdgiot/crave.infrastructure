﻿namespace Bootstrapper.Models
{
    public class CraveGoogleClient
    {
        public static CraveGoogleClient GetClientForCognito() => new CraveGoogleClient(
            "820191028665-tu9f6p4gjb0gp1gj0rs8f7f13s2jma01.apps.googleusercontent.com",
            "tGW9s0CndEZ-hGCicxwhclD8");

        public CraveGoogleClient(string id, string secret)
        {
            Id = id;
            Secret = secret;
        }

        public string Id { get; }
        public string Secret { get; }
    }
}
