using Amazon.CDK;
using Amazon.CDK.AWS.XRay;
using Constructs;
using Crave.Infrastructure.Models;
using static Amazon.CDK.AWS.XRay.CfnSamplingRule;

namespace Bootstrapper.Stacks.ApiGateway
{
    public class CraveXRaySamplingRules : Construct
    {
        public class CraveXRaySamplingRulesProps : StackProps
        {
            public CraveEnvironment CraveEnv { get; set; }
        }

        public CraveXRaySamplingRules(Construct scope, CraveXRaySamplingRulesProps props) : base(scope, "crave-x-ray-sampling-rules")
        {
            _ = new CfnSamplingRule(this, "sampling-rule-exlude-health", new CfnSamplingRuleProps
            {
                SamplingRule = new SamplingRuleProperty
                {
                    RuleName = "ExcludeHealth",
                    Priority = 25,
                    ReservoirSize = 0,
                    FixedRate = 0,
                    ServiceName = "*",
                    ServiceType = "*",
                    HttpMethod = "*",
                    UrlPath = "*/health",
                    ResourceArn = "*",
                    Host = "*",
                    Version = 1
                }
            });

            _ = new CfnSamplingRule(this, "sampling-rule-exlude-pings", new CfnSamplingRuleProps
            {
                SamplingRule = new SamplingRuleProperty
                {
                    RuleName = "ExcludePings",
                    Priority = 50,
                    ReservoirSize = 0,
                    FixedRate = 0,
                    ServiceName = "*",
                    ServiceType = "*",
                    HttpMethod = "*",
                    UrlPath = "*/ping",
                    ResourceArn = "*",
                    Host = "*",
                    Version = 1
                }
            });

            _ = new CfnSamplingRule(this, "sampling-rule-exlude-netmessages", new CfnSamplingRuleProps
            {
                SamplingRule = new SamplingRuleProperty
                {
                    RuleName = "ExcludeNetmessages",
                    Priority = 100,
                    ReservoirSize = 0,
                    FixedRate = 0,
                    ServiceName = "*",
                    ServiceType = "*",
                    HttpMethod = "*",
                    UrlPath = "*/netmessages/*",
                    ResourceArn = "*",
                    Host = "*",
                    Version = 1
                }
            });

            _ = new CfnSamplingRule(this, "sampling-rule-crave", new CfnSamplingRuleProps
            {
                SamplingRule = new SamplingRuleProperty
                {
                    RuleName = "Crave",
                    Priority = 1000,
                    ReservoirSize = 1000000,
                    FixedRate = 1,
                    ServiceName = "*",
                    ServiceType = "*",
                    HttpMethod = "*",
                    UrlPath = "*",
                    ResourceArn = "*",
                    Host = "*",
                    Version = 1
                }
            });
        }
    }
}