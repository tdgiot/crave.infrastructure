﻿using System.Linq;
using Amazon.CDK;
using Amazon.CDK.AWS.EC2;
using Amazon.CDK.AWS.ElastiCache;
using Amazon.CDK.AWS.Route53;
using Constructs;
using Crave.Infrastructure;
using Crave.Infrastructure.Models;

namespace Bootstrapper.Stacks.ElastiCache
{
    public class RedisCacheStack : Stack
    {
        public class RedisCacheStackProps : StackProps
        {
            public CraveEnvironment CraveEnv { get; set; }
        }

        public RedisCacheStack(Construct scope, string id, RedisCacheStackProps props) : base(scope, id, props)
        {
            IVpc vpc = Vpc.FromLookup(this, "vpc", new VpcLookupOptions
            {
                VpcName = CraveVpcs.CraveVpc.VpcName
            });

            CfnSubnetGroup redisSubnetGroup = new CfnSubnetGroup(this, "redis-private-subnetgroup",
                new CfnSubnetGroupProps
                {
                    CacheSubnetGroupName = "private-subnet-group",
                    SubnetIds = vpc.PrivateSubnets.Select(x => x.SubnetId).ToArray(),
                    Description = "Private subnet group"
                });

            SecurityGroup elastiCacheSecurityGroup = new SecurityGroup(this, "redis-security-group",
                new SecurityGroupProps
                {
                    SecurityGroupName = "elasticache-redis-security-group",
                    Description = "ElastiCache Redis Security Group",
                    Vpc = vpc,
                    AllowAllOutbound = true
                });
            elastiCacheSecurityGroup.AddIngressRule(Peer.AnyIpv4(), Port.Tcp(6379), "Redis Inbound");

            // https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-elasticache-cache-cluster.html
            CfnCacheCluster redisCluster = new CfnCacheCluster(this, "redis-cache-cluster", new CfnCacheClusterProps
            {
                ClusterName = "redis-cluster",
                Engine = "redis",
                AzMode = "single-az",
                CacheNodeType = "cache.t2.micro",
                CacheSubnetGroupName = redisSubnetGroup.Ref,
                VpcSecurityGroupIds = new[] { elastiCacheSecurityGroup.SecurityGroupId },
                NumCacheNodes = 1,
                AutoMinorVersionUpgrade = true
            });

            redisCluster.AddDependsOn(redisSubnetGroup);

            // Create internal DNS records pointing to the database instance
            IHostedZone hostedZone = HostedZone.FromLookup(this, "hosted-zone", new HostedZoneProviderProps
            {
                DomainName = props.CraveEnv.DomainNames.Internal,
                PrivateZone = true
            });

            // IPv4
            _ = new RecordSet(this, "dns-cname-record", new RecordSetProps
            {
                Zone = hostedZone,
                RecordName = "redis",
                RecordType = RecordType.CNAME,
                Target = RecordTarget.FromValues(redisCluster.AttrRedisEndpointAddress)
            });
        }
    }
}