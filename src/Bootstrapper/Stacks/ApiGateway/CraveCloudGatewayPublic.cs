﻿using Amazon.CDK;
using Amazon.CDK.AWS.APIGateway;
using Amazon.CDK.AWS.CertificateManager;
using Amazon.CDK.AWS.Route53;
using Amazon.CDK.AWS.Route53.Targets;
using Constructs;
using Crave.Infrastructure;
using Crave.Infrastructure.Models;

namespace Bootstrapper.Stacks.ApiGateway
{
    public class CraveCloudGatewayPublic : Construct
    {
        public class CraveCloudGatewayPublicProps : StackProps
        {
            public CraveEnvironment CraveEnv { get; set; }
        }

        public CraveCloudGatewayPublic(Construct scope, CraveCloudGatewayPublicProps props) : base(scope, "CraveCloud-ApiGatewayDomain-Public")
        {
            string appName = "gateway";
            string domainName = $"{appName}.{props.CraveEnv.DomainNames.External}";

            ICertificate gatewayCert = Certificate.FromCertificateArn(this, "public-gateway-cert", CraveCertificates.GetCraveCloudXyzCertificateArn(props.CraveEnv.Environment));

            DomainName_ gatewayDomain = new DomainName_(this, "public-gateway-domain", new DomainNameProps
            {
                DomainName = domainName,
                EndpointType = EndpointType.REGIONAL,
                Certificate = gatewayCert,
                SecurityPolicy = SecurityPolicy.TLS_1_2
            });
            gatewayDomain.ApplyRemovalPolicy(RemovalPolicy.RETAIN);

            IHostedZone hostedZone = HostedZone.FromLookup(this, "hosted-zone", new HostedZoneProviderProps
            {
                DomainName = props.CraveEnv.DomainNames.External
            });

            // IPv4
            _ = new RecordSet(this, "gateway-a-record", new RecordSetProps
            {
                Zone = hostedZone,
                RecordName = appName.ToLower(),
                RecordType = RecordType.A,
                Target = RecordTarget.FromAlias(new ApiGatewayDomain(gatewayDomain))
            });
            // IPv6
            _ = new RecordSet(this, "gateway-aaaa-record", new RecordSetProps
            {
                Zone = hostedZone,
                RecordName = appName.ToLower(),
                RecordType = RecordType.AAAA,
                Target = RecordTarget.FromAlias(new ApiGatewayDomain(gatewayDomain))
            });
        }
    }
}