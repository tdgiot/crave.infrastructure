﻿using Amazon.CDK;
using Amazon.CDK.AWS.EC2;
using Amazon.CDK.AWS.RDS;
using Amazon.CDK.AWS.Route53;
using Constructs;
using Crave.Infrastructure;
using Crave.Infrastructure.Enums;
using Crave.Infrastructure.Models;

namespace Bootstrapper.Stacks.RDS
{
    public class MSSQLDatabaseStack : Stack
    {
        public class MSSQLDatabaseProps : StackProps
        {
            public CraveEnvironment CraveEnv { get; set; }
        }

        public MSSQLDatabaseStack(Construct scope, string id, MSSQLDatabaseProps props) : base(scope, id, props)
        {
            IVpc vpc = Vpc.FromLookup(this, "vpc", new VpcLookupOptions
            {
                VpcName = CraveVpcs.CraveVpc.VpcName
            });

            DatabaseInstance instance = new DatabaseInstance(this, "db-instance", new DatabaseInstanceProps
            {
                Engine = DatabaseInstanceEngine.SqlServerSe(new SqlServerSeInstanceEngineProps
                {
                    Version = SqlServerEngineVersion.VER_15
                }),
                InstanceType = InstanceType.Of(InstanceClass.MEMORY5, InstanceSize.LARGE),
                Credentials = Credentials.FromGeneratedSecret("CraveAdmin"),
                AllocatedStorage = 250,
                MaxAllocatedStorage = 500,
                LicenseModel = LicenseModel.LICENSE_INCLUDED,
                MultiAz = false,
                Vpc = vpc,
                VpcSubnets = new SubnetSelection
                {
                    SubnetType = SubnetType.PRIVATE_WITH_NAT
                },
                StorageEncrypted = true,
                BackupRetention = Duration.Days(props.CraveEnv.Environment == EnvironmentType.Playground ? 30 : 1)
            });

            _ = new CfnOutput(this, "RDS-Crave-Output", new CfnOutputProps
            {
                Value = Token.AsString(instance.InstanceEndpoint.SocketAddress)
            });


            // Create internal DNS records pointing to the database instance
            IHostedZone hostedZone = HostedZone.FromLookup(this, "hosted-zone", new HostedZoneProviderProps
            {
                DomainName = props.CraveEnv.DomainNames.Internal,
                PrivateZone = true
            });

            // IPv4
            _ = new RecordSet(this, "gateway-a-record", new RecordSetProps
            {
                Zone = hostedZone,
                RecordName = "crave-db",
                RecordType = RecordType.CNAME,
                Target = RecordTarget.FromIpAddresses(instance.InstanceEndpoint.Hostname)
            });
        }
    }
}