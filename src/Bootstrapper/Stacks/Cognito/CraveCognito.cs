﻿using Amazon.CDK;
using Amazon.CDK.AWS.Cognito;
using Amazon.CDK.AWS.SSM;
using Constructs;
using Crave.Infrastructure;
using Crave.Infrastructure.Models;

namespace Bootstrapper.Stacks.Cognito
{
    public class CraveCognito : Construct
    {
        public class CraveCognitoProps : StackProps
        {
            public CraveEnvironment CraveEnv { get; set; }
        }

        public CraveCognito(Construct scope, CraveCognitoProps props) : base(scope, "crave-cognito")
        {
            CraveUserPool craveUserPool = new CraveUserPool(this, "crave-user-pool", new CraveUserPool.CraveUserPoolProps
            {
                CraveEnv = props.CraveEnv
            });

            CraveIdentityPool craveIdentityPool = new CraveIdentityPool(this, "crave-identity-pool", new CraveIdentityPool.CraveIdentityPoolProps
            {
                Region = props.CraveEnv.Region,
                UserPoolId = craveUserPool.UserPool.UserPoolId,
                AppClientId = craveUserPool.AppClientId,
                UserPoolProviderName = craveUserPool.UserPool.UserPoolProviderName
            });

            _ = new CraveUserPoolGroups(this, "crave-user-pool-groups", new CraveUserPoolGroups.CraveUserPoolGroupsProps
            {
                UserPoolId = craveUserPool.UserPool.UserPoolId,
                IdentityPoolRef = craveIdentityPool.IdentityPool.Ref
            });

            craveIdentityPool.IdentityPool.AddDependsOn(craveUserPool.UserPool.Node.DefaultChild as CfnUserPool);

            _ = new StringParameter(this, "crave-param-user-pool-id", new StringParameterProps
            {
                ParameterName = CraveParameters.UserPool.UserPoolId,
                StringValue = craveUserPool.UserPool.UserPoolId
            });

            _ = new StringParameter(this, "crave-param-identity-pool-id", new StringParameterProps
            {
                ParameterName = CraveParameters.UserPool.IdentityPoolId,
                StringValue = craveIdentityPool.IdentityPool.Ref
            });

            _ = new StringParameter(this, "crave-param-app-client-id", new StringParameterProps
            {
                ParameterName = CraveParameters.UserPool.AppClientId,
                StringValue = craveUserPool.AppClientId
            });
        }
    }
}
