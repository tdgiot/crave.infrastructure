using System.Collections.Generic;
using Amazon.CDK;
using Amazon.CDK.AWS.Cognito;
using Amazon.CDK.AWS.IAM;
using Constructs;
using static Amazon.CDK.AWS.Cognito.CfnIdentityPool;

namespace Bootstrapper.Stacks.Cognito
{
    internal class CraveIdentityPool : Construct
    {
        public class CraveIdentityPoolProps : StackProps
        {
            public string Region { get; set; }
            public string UserPoolId { get; set; }
            public string AppClientId { get; set; }
            public string UserPoolProviderName { get; set; }
        }

        public CfnIdentityPool IdentityPool { get; }

        public CraveIdentityPool(Construct scope, string id, CraveIdentityPoolProps props) : base(scope, id)
        {
            IdentityPool = new CfnIdentityPool(this, "crave-identity-pool", new CfnIdentityPoolProps
            {
                AllowUnauthenticatedIdentities = false,
                CognitoIdentityProviders = new[]
                {
                    new CognitoIdentityProviderProperty
                    {
                        ClientId = props.AppClientId,
                        ProviderName = props.UserPoolProviderName
                    }
                }
            });

            Role authenticatedRole = CreateAuthenticatedRole(IdentityPool);
            Role unauthenticatedRole = CreateUnauthenticatedRole(IdentityPool);

            CfnIdentityPoolRoleAttachment cfnIdentityPoolRoleAttachment = new CfnIdentityPoolRoleAttachment(this, "crave-identity-pool-role-attachment", new CfnIdentityPoolRoleAttachmentProps
            {
                IdentityPoolId = IdentityPool.Ref,
                Roles = new Dictionary<string, object>
                {
                    ["unauthenticated"] = unauthenticatedRole.RoleArn,
                    ["authenticated"] = authenticatedRole.RoleArn
                },
                RoleMappings = new Dictionary<string, object>
                {
                    ["mapping"] = new CfnIdentityPoolRoleAttachment.RoleMappingProperty
                    {
                        IdentityProvider = $"cognito-idp.{props.Region}.amazonaws.com/{props.UserPoolId}:{props.AppClientId}",
                        Type = "Token",
                        AmbiguousRoleResolution = "AuthenticatedRole"
                    }
                }
            });

            cfnIdentityPoolRoleAttachment.AddDependsOn(IdentityPool);
        }

        private Role CreateAuthenticatedRole(CfnIdentityPool cfnIdentityPool)
        {
            Role role = new Role(this, "cognito-identity-pool-role-authenticated", new RoleProps
            {
                RoleName = "crave-cognito-identity-pool-authenticated",
                Description = "Identity pool authenticated role",
                AssumedBy = new FederatedPrincipal(
                    "cognito-identity.amazonaws.com",
                    new Dictionary<string, object>
                    {
                        ["StringEquals"] = new Dictionary<string, object>
                        {
                            ["cognito-identity.amazonaws.com:aud"] = cfnIdentityPool.Ref
                        },
                        ["ForAnyValue:StringLike"] = new Dictionary<string, object>
                        {
                            ["cognito-identity.amazonaws.com:amr"] = "authenticated"
                        }
                    },
                    "sts:AssumeRoleWithWebIdentity")
            });

            role.AddToPolicy(new PolicyStatement(new PolicyStatementProps
            {
                Effect = Effect.ALLOW,
                Actions = new[]
                {
                    "mobileanalytics:PutEvents",
                    "cognito-sync:*",
                    "cognito-identity:*"
                },
                Resources = new[]
                {
                    "*"
                }
            }));

            return role;
        }

        private Role CreateUnauthenticatedRole(CfnIdentityPool cfnIdentityPool)
        {
            Role role = new Role(this, "cognito-identity-pool-role-unauthenticated", new RoleProps
            {
                RoleName = "crave-cognito-identity-pool-unauthenticated",
                Description = "Identity pool unauthenticated role",
                AssumedBy = new FederatedPrincipal(
                    "cognito-identity.amazonaws.com",
                    new Dictionary<string, object>
                    {
                        ["StringEquals"] = new Dictionary<string, object>
                        {
                            ["cognito-identity.amazonaws.com:aud"] = cfnIdentityPool.Ref
                        },
                        ["ForAnyValue:StringLike"] = new Dictionary<string, object>
                        {
                            ["cognito-identity.amazonaws.com:amr"] = "unauthenticated"
                        }
                    },
                    "sts:AssumeRoleWithWebIdentity")
            });

            role.AddToPolicy(new PolicyStatement(new PolicyStatementProps
            {
                Effect = Effect.ALLOW,
                Actions = new[]
                {
                    "mobileanalytics:PutEvents",
                    "cognito-sync:*"
                },
                Resources = new[]
                {
                    "*"
                }
            }));

            return role;
        }
    }
}
