﻿using Amazon.CDK;
using Amazon.CDK.AWS.IAM;
using Amazon.CDK.AWS.Lambda;
using Constructs;

namespace Bootstrapper.Stacks.Cognito
{
    public class CravePostConfirmationLambda : Construct
    {
        public IFunction Function { get; }

        public CravePostConfirmationLambda(Construct scope) : base(scope, "crave-post-confirmation-lambda")
        {
            Role postConfirmationRole = new Role(this, "crave-cognito-post-confirmation-role", new RoleProps
            {
                RoleName = "crave-cognito-post-confirmation-role",
                Description = "Role for executing post confirmation for newly added users",
                AssumedBy = new ServicePrincipal("lambda.amazonaws.com")
            });

            postConfirmationRole.AddToPolicy(new PolicyStatement(new PolicyStatementProps
            {
                Effect = Effect.ALLOW,
                Actions = new[]
                {
                    "cognito-identity:*",
                    "cognito-sync:*",
                    "cognito-idp:*"
                },
                Resources = new[]
                {
                    "*"
                }
            }));

            this.Function = new Function(this, "crave-cognito-post-confirmation", new FunctionProps
            {
                Runtime = Runtime.NODEJS_14_X,
                Role = postConfirmationRole,
                Code = Code.FromAsset("src/Bootstrapper/Assets/Post-Confirmation"),
                Handler = "post-confirmation.addUserToGroup"
            });
        }
    }
}
