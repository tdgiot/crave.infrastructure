using System.Collections.Generic;
using Amazon.CDK.AWS.Cognito;
using Amazon.CDK.AWS.IAM;
using Constructs;

namespace Bootstrapper.Stacks.Cognito
{
    internal class CraveUserPoolGroups : Construct
    {
        public class CraveUserPoolGroupsProps
        {
            public string UserPoolId { get; set; }
            public string IdentityPoolRef { get; set; }
        }

        public CraveUserPoolGroups(Construct scope, string id, CraveUserPoolGroupsProps props) : base(scope, id)
        {
            Role adminRole = CreateAdminRole(props.IdentityPoolRef);

            _ = new CfnUserPoolGroup(this, "crave-group-admin", new CfnUserPoolGroupProps
            {
                UserPoolId = props.UserPoolId,
                GroupName = "Admin",
                RoleArn = adminRole.RoleArn
            });

            _ = new CfnUserPoolGroup(this, "crave-group-user", new CfnUserPoolGroupProps
            {
                UserPoolId = props.UserPoolId,
                GroupName = "User"
            });
        }

        private Role CreateAdminRole(string identityPoolRef)
        {
            Role role = new Role(this, "cognito-user-pool-role-group-admin", new RoleProps
            {
                RoleName = "crave-cognito-user-pool-group-admin",
                Description = "Cognito user pool Admin group role",
                AssumedBy = new FederatedPrincipal(
                    "cognito-identity.amazonaws.com",
                    new Dictionary<string, object>
                    {
                        ["StringEquals"] = new Dictionary<string, object>
                        {
                            ["cognito-identity.amazonaws.com:aud"] = identityPoolRef
                        }
                    },
                    "sts:AssumeRoleWithWebIdentity"),
            });

            role.AddToPolicy(new PolicyStatement(new PolicyStatementProps
            {
                Effect = Effect.ALLOW,
                Actions = new string[]
                {
                    "cognito-sync:*",
                    "cognito-identity:*",
                    "cognito-idp:ListUsers",
                    "cognito-idp:AdminCreateUser",
                    "cognito-idp:AdminDeleteUser",
                    "cognito-idp:AdminUpdateUserAttributes"
                },
                Resources = new string[]
                {
                    "*"
                }
            }));

            return role;
        }
    }
}
