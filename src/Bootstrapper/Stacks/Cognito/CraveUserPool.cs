using System.Collections.Generic;
using System.Linq;
using Amazon.CDK;
using Amazon.CDK.AWS.Cognito;
using Bootstrapper.Models;
using Constructs;
using Crave.Infrastructure.Enums;
using Crave.Infrastructure.Models;

namespace Bootstrapper.Stacks.Cognito
{
    internal class CraveUserPool : Construct
    {
        public class CraveUserPoolProps
        {
            public CraveEnvironment CraveEnv { get; set; }
        }

        public UserPool UserPool { get; }
        public string AppClientId { get; set; }

        public CraveUserPool(Construct scope, string id, CraveUserPoolProps props) : base(scope, id)
        {
            CravePostConfirmationLambda postConfirmationLambda = new CravePostConfirmationLambda(this);
            //CravePreSignupLambda preSignupLambda = new CravePreSignupLambda(this);

            UserPool = new UserPool(this, "crave-user-pool", new UserPoolProps
            {
                UserPoolName = "CraveUserPool",
                SelfSignUpEnabled = false,
                SignInCaseSensitive = false,
                SignInAliases = new SignInAliases
                {
                    Email = true
                },
                StandardAttributes = new StandardAttributes
                {
                    Email = new StandardAttribute
                    {
                        Mutable = true,
                        Required = true
                    }
                },
                PasswordPolicy = new PasswordPolicy
                {
                    MinLength = 10,
                    RequireDigits = false,
                    RequireSymbols = false,
                    RequireUppercase = false,
                    RequireLowercase = false
                },
                UserVerification = new UserVerificationConfig
                {
                    EmailStyle = VerificationEmailStyle.CODE,
                    EmailSubject = "Your verification code",
                    EmailBody = "Your verification code is {####}."
                },
                UserInvitation = new UserInvitationConfig
                {
                    EmailSubject = "You're invited to the Crave Back Office!",
                    EmailBody = "Welcome, your username is {username} and temporary password is {####}."
                },
                LambdaTriggers = new UserPoolTriggers
                {
                    PostConfirmation = postConfirmationLambda.Function,
                    //PreSignUp = preSignupLambda.Function
                }
            });

            UserPool.AddDomain("crave-user-pool-domain", new UserPoolDomainProps
            {
                CognitoDomain = new CognitoDomainOptions
                {
                    DomainPrefix = $"crave-{props.CraveEnv.Environment}".ToLowerInvariant()
                }
            });

            //CraveGoogleClient craveGoogleClient = CraveGoogleClient.GetClientForCognito();

            //UserPoolIdentityProviderGoogle userPoolIdentityProviderGoogle = new UserPoolIdentityProviderGoogle(this, "user-pool-identity-provider-google", new UserPoolIdentityProviderGoogleProps
            //{
            //    UserPool = UserPool,
            //    ClientId = craveGoogleClient.Id,
            //    ClientSecret = craveGoogleClient.Secret,
            //    Scopes = new[]
            //    {
            //        "profile",
            //        "email",
            //        "openid"
            //    },
            //    AttributeMapping = new AttributeMapping
            //    {
            //        Email = ProviderAttribute.GOOGLE_EMAIL,
            //        ProfilePicture = ProviderAttribute.GOOGLE_PICTURE,
            //        GivenName = ProviderAttribute.GOOGLE_GIVEN_NAME,
            //        FamilyName = ProviderAttribute.GOOGLE_FAMILY_NAME
            //    }
            //});

            IList<string> callbackUrls = new List<string> { $"https://backoffice.{props.CraveEnv.DomainNames.External}/pages/dashboard" };
            IList<string> logoutUrls = new List<string> { $"https://backoffice.{props.CraveEnv.DomainNames.External}/auth/login" };

            if (props.CraveEnv.Environment == EnvironmentType.Development)
            {
                callbackUrls.Add("http://localhost:4200/pages/dashboard");
                logoutUrls.Add("http://localhost:4200/auth/login");
            }

            UserPoolClient userPoolClient = UserPool.AddClient("BackOfficeAngularClient", new UserPoolClientOptions
            {
                UserPoolClientName = "BackOfficeAngularClient",
                GenerateSecret = false,
                RefreshTokenValidity = Duration.Hours(2),
                AccessTokenValidity = Duration.Minutes(15),
                IdTokenValidity = Duration.Minutes(15),
                AuthFlows = new AuthFlow
                {
                    AdminUserPassword = true,
                    UserSrp = true
                },
                SupportedIdentityProviders = new[]
                {
                    //UserPoolClientIdentityProvider.GOOGLE,
                    UserPoolClientIdentityProvider.COGNITO
                },
                OAuth = new OAuthSettings
                {
                    CallbackUrls = callbackUrls.ToArray(),
                    LogoutUrls = logoutUrls.ToArray(),
                    Flows = new OAuthFlows
                    {
                        AuthorizationCodeGrant = true
                    },
                    Scopes = new[]
                    {
                        OAuthScope.EMAIL,
                        OAuthScope.OPENID,
                        OAuthScope.COGNITO_ADMIN,
                        OAuthScope.PROFILE
                    }
                }
            });

            AppClientId = userPoolClient.UserPoolClientId;

            //CfnUserPoolClient cfnUserPoolClient = userPoolClient.Node.DefaultChild as CfnUserPoolClient;
            //cfnUserPoolClient.AddDependsOn(userPoolIdentityProviderGoogle.Node.DefaultChild as CfnUserPoolIdentityProvider);

            string cmsUrl = props.CraveEnv.Environment switch
            {
                EnvironmentType.Production => "https://cms.prod.trueguest.tech/",
                EnvironmentType.Development => "https://cms.dev.trueguest.tech/",
                _ => "http://localhost/trunk/management/",
            };

            UserPoolClient cmsClient = UserPool.AddClient("CmsClient", new UserPoolClientOptions
            {
                UserPoolClientName = "CmsClient",
                GenerateSecret = true,
                RefreshTokenValidity = Duration.Hours(2),
                AccessTokenValidity = Duration.Minutes(15),
                IdTokenValidity = Duration.Minutes(15),
                AuthFlows = new AuthFlow
                {
                    AdminUserPassword = true,
                    UserSrp = true

                },
                SupportedIdentityProviders = new[]
                 {
                    //UserPoolClientIdentityProvider.GOOGLE,
                    UserPoolClientIdentityProvider.COGNITO
                },
                OAuth = new OAuthSettings
                {
                    CallbackUrls = new List<string> { cmsUrl + "CognitoOAuth.aspx" }.ToArray(),
                    LogoutUrls = new string[0],
                    Flows = new OAuthFlows
                    {
                        AuthorizationCodeGrant = true
                    },
                    Scopes = new[]
                     {
                        OAuthScope.EMAIL,
                        OAuthScope.OPENID,
                        OAuthScope.PROFILE
                    }
                }
            });

            //CfnUserPoolClient cfnUserPoolCmsClient = cmsClient.Node.DefaultChild as CfnUserPoolClient;
            //cfnUserPoolCmsClient.AddDependsOn(userPoolIdentityProviderGoogle.Node.DefaultChild as CfnUserPoolIdentityProvider);
        }
    }
}
