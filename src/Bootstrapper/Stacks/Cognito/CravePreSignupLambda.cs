﻿using Amazon.CDK;
using Amazon.CDK.AWS.IAM;
using Amazon.CDK.AWS.Lambda;
using Constructs;

namespace Bootstrapper.Stacks.Cognito
{
    public class CravePreSignupLambda : Construct
    {
        public IFunction Function { get; }

        public CravePreSignupLambda(Construct scope) : base(scope, "crave-pre-sign-up-lambda")
        {
            Role preSignUpRole = new Role(this, "crave-cognito-pre-sign-up-role", new RoleProps
            {
                RoleName = "crave-cognito-pre-sign-up-role",
                Description = "Role for executing pre sign-up confirmation for new users",
                AssumedBy = new ServicePrincipal("lambda.amazonaws.com")
            });

            preSignUpRole.AddToPolicy(new PolicyStatement(new PolicyStatementProps
            {
                Effect = Effect.ALLOW,
                Actions = new[]
                {
                    "cognito-identity:*",
                    "cognito-sync:*",
                    "cognito-idp:*"
                },
                Resources = new[]
                {
                    "*"
                }
            }));

            this.Function = new Function(this, "crave-cognito-pre-sign-up", new FunctionProps
            {
                Runtime = Runtime.NODEJS_14_X,
                Role = preSignUpRole,
                Code = Code.FromAsset("src/Bootstrapper/Assets/Pre-Sign-Up"),
                Handler = "pre-sign-up.preSignUp"
            });
        }
    }
}
