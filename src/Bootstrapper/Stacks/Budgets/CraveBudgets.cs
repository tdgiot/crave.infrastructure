﻿using Amazon.CDK;
using Bootstrapper.Stacks.Budgets.Budget;
using Constructs;

namespace Bootstrapper.Stacks.IAM
{
    public class CraveBudgets : Stack
    {
        public CraveBudgets(Construct scope, string id, StackProps props) : base(scope, id, props)
        {
            _ = new WafBudget(this, "waf-budget");
        }
    }
}
