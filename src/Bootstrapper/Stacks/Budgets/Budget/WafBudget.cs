﻿using Amazon.CDK.AWS.Budgets;
using Constructs;
using Newtonsoft.Json;
using static Amazon.CDK.AWS.Budgets.CfnBudget;

namespace Bootstrapper.Stacks.Budgets.Budget
{
    internal class WafBudget : Construct
    {
        public WafBudget(Construct scope, string id) : base(scope, id)
        {
            _ = new CfnBudget(this, "waf-budget", new CfnBudgetProps
            {
                Budget = new BudgetDataProperty
                {
                    BudgetName = "WAF Budget",
                    BudgetType = "COST",
                    TimeUnit = "MONTHLY",
                    BudgetLimit = new SpendProperty
                    {
                        Amount = 25,
                        Unit = "USD"
                    },
                    CostFilters = JsonConvert.DeserializeObject("{ \"Service\": [\"WAF\"] }")
                },
                NotificationsWithSubscribers = new NotificationWithSubscribersProperty[]
                {
                    new NotificationWithSubscribersProperty
                    {
                        Notification = new NotificationProperty
                        {
                            NotificationType = "ACTUAL",
                            ComparisonOperator = "GREATER_THAN",
                            Threshold = 80
                        },
                        Subscribers = new SubscriberProperty[]
                        {
                            new SubscriberProperty
                            {
                                SubscriptionType = "EMAIL",
                                Address = "crave.tdg@gmail.com"
                            }
                        }
                    }
                }
            });
        }
    }
}
