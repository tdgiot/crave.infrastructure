﻿using Amazon.CDK;
using Amazon.CDK.AWS.IAM;
using Bootstrapper.Stacks.IAM.Policies;
using Constructs;
using Crave.Infrastructure;

namespace Bootstrapper.Stacks.IAM
{
    public class CraveManagedRoles : Stack
    {
        public CraveManagedRoles(Construct scope, string id, StackProps props) : base(scope, id, props)
        {
            ManagedPolicy craveLogPolicy = new WriteToCraveCloudWatchLogGroupPolicy(this, "cloudwatch-log-policy");

            _ = new Role(this, "BeanstalkEC2WebRole", new RoleProps
            {
                AssumedBy = new ServicePrincipal("ec2.amazonaws.com"),
                RoleName = CraveRole.DEFAULT_BEANSTALK_WEB_TIER_ROLE,
                Description = "Default role for web-tier Elastic Beanstalk applications",
                ManagedPolicies = new[]
                {
                    ManagedPolicy.FromAwsManagedPolicyName("AWSElasticBeanstalkWebTier"),
                    craveLogPolicy
                }
            });

            _ = new Role(this, "BeanstalkEC2WorkerRole", new RoleProps
            {
                AssumedBy = new ServicePrincipal("ec2.amazonaws.com"),
                RoleName = CraveRole.DEFAULT_BEANSTALK_WORKER_ROLE,
                Description = "Default role for Elastic Beanstalk worker applications",
                ManagedPolicies = new[]
                {
                    ManagedPolicy.FromAwsManagedPolicyName("AWSElasticBeanstalkWorkerTier"),
                    craveLogPolicy
                }
            });

            // Create new role used by the beanstalk environment
            _ = new Role(this, "BeanstalkServiceRole", new RoleProps
            {
                AssumedBy = new ServicePrincipal("elasticbeanstalk.amazonaws.com"),
                RoleName = CraveRole.DEFAULT_BEANSTALK_SERVICE_ROLE,
                Description = "Default Elastic Beanstalk service role",
                ManagedPolicies = new[]
                {
                    ManagedPolicy.FromAwsManagedPolicyName("service-role/AWSElasticBeanstalkEnhancedHealth"),
                    ManagedPolicy.FromAwsManagedPolicyName("service-role/AWSElasticBeanstalkService")
                }
            });

            // ROLE: Grafana
            GrafanaDashboardPolicy grafanaDashboardPolicy = new(this, "grafana-cloudwatch-policy");
            _ = new Role(this, "crave-grafana-role", new RoleProps
            {
                AssumedBy = new AccountPrincipal(CraveAWSAccounts.Crave_IAM),
                RoleName = CraveRole.CRAVE_GRAFANA_ROLE,
                Description = "Role to grant the Grafana user rights to CloudWatch",
                ManagedPolicies = new[]
                {
                    grafanaDashboardPolicy
                }
            });
        }
    }
}