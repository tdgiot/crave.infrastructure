﻿using Amazon.CDK.AWS.IAM;
using Constructs;
using Crave.Infrastructure;

namespace Bootstrapper.Stacks.IAM.Policies
{
    internal class GrafanaDashboardPolicy : ManagedPolicy
    {
        public GrafanaDashboardPolicy(Construct scope, string id) : base(scope, id, new PolicyProps())
        {
        }

        private class PolicyProps : ManagedPolicyProps
        {
            internal PolicyProps()
            {
                this.ManagedPolicyName = CraveManagedPolicy.GRAFANA_DASHBOARD_POLICY;
                this.Description = "Policy to allow the grafana user to read cloudwatch";
                this.Statements = new[]
                {
                    new PolicyStatement(new PolicyStatementProps
                    {
                        Sid = "AllowReadingMetricsFromCloudWatch",
                        Effect = Effect.ALLOW,
                        Actions = new[]
                        {
                            "cloudwatch:DescribeAlarmsForMetric",
                            "cloudwatch:DescribeAlarmHistory",
                            "cloudwatch:DescribeAlarms",
                            "cloudwatch:ListMetrics",
                            "cloudwatch:GetMetricStatistics",
                            "cloudwatch:GetMetricData"
                        },
                        Resources = new[]
                        {
                            "*"
                        }
                    }),
                    new PolicyStatement(new PolicyStatementProps
                    {
                        Sid = "AllowReadingLogsFromCloudWatch",
                        Effect= Effect.ALLOW,
                        Actions= new[]
                        {
                            "logs:DescribeLogGroups",
                            "logs:GetLogGroupFields",
                            "logs:StartQuery",
                            "logs:StopQuery",
                            "logs:GetQueryResults",
                            "logs:GetLogEvents"
                        },
                        Resources = new[]
                        {
                            "*"
                        }
                    }),
                    new PolicyStatement(new PolicyStatementProps
                    {
                        Sid= "AllowReadingTagsInstanceRegionsFromEC2",
                        Effect = Effect.ALLOW,
                        Actions = new []
                        {
                            "ec2:DescribeTags",
                            "ec2:DescribeInstances",
                            "ec2:DescribeRegions"
                        },
                        Resources = new[]
                        {
                            "*"
                        }
                    }),
                    new PolicyStatement(new PolicyStatementProps
                    {
                        Sid = "AllowReadingResourcesForTags",
                        Effect = Effect.ALLOW,
                        Actions = new []
                        {
                            "tags:GetResources"
                        },
                        Resources = new[]
                        {
                            "*"
                        }
                    })
                };
            }
        }
    }
}
