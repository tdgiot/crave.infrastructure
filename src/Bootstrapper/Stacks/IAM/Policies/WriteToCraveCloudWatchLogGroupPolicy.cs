﻿using Amazon.CDK.AWS.IAM;
using Constructs;
using Crave.Infrastructure;

namespace Bootstrapper.Stacks.IAM.Policies
{
    internal class WriteToCraveCloudWatchLogGroupPolicy : ManagedPolicy
    {
        public WriteToCraveCloudWatchLogGroupPolicy(Construct scope, string id) : base(scope, id, new PolicyProps())
        {
        }

        private class PolicyProps : ManagedPolicyProps
        {
            internal PolicyProps()
            {
                this.ManagedPolicyName = CraveManagedPolicy.CLOUDWATCH_LOG_WRITE;
                this.Description = "Policy to allow resource to created and write to '/crave/*' log groups.";
                this.Statements = new[]
                {
                    new PolicyStatement(new PolicyStatementProps
                    {
                        Effect = Effect.ALLOW,
                        Actions = new []
                        {
                            "logs:CreateLogDelivery",
                            "logs:CreateLogGroup",
                            "logs:CreateLogStream",
                            "logs:DescribeLogGroups",
                            "logs:DescribeLogStreams",
                            "logs:DescribeExportTasks",
                            "logs:DescribeDestinations",
                            "logs:GetLogEvents",
                            "logs:GetLogGroupFields",
                            "logs:GetLogRecord",
                            "logs:GetLogDelivery",
                            "logs:PutRetentionPolicy",
                            "logs:PutLogEvents",
                            "logs:PutResourcePolicy"
                        },
                        Resources = new []
                        {
                            "arn:aws:logs:*:*:log-group:*",
                            "arn:aws:logs:*:*:log-group:/crave/*:log-stream:*"
                        }
                    })
                };
            }
        }
    }
}
