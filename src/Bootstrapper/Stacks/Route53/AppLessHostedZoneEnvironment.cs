﻿using System;
using Amazon.CDK;
using Amazon.CDK.AWS.CertificateManager;
using Amazon.CDK.AWS.Route53;
using Amazon.CDK.AWS.SSM;
using Constructs;
using Crave.Infrastructure;
using Crave.Infrastructure.Enums;

namespace Bootstrapper.Stacks.Route53
{
    public class AppLessHostedZoneEnvironment : Stack
    {
        private const string ZONE_NAME_BASE = "appless.app";

        public class AppLessHostedZoneEnvironmentProps : StackProps
        {
            public EnvironmentType Environment { get; set; }
        }

        public AppLessHostedZoneEnvironment(Construct scope, string id, AppLessHostedZoneEnvironmentProps props) : base(scope, id, props)
        {
            string zoneName = GenerateZoneName(props.Environment, ZONE_NAME_BASE);
            PublicHostedZone publicHostedZone = new PublicHostedZone(this, $"PublicZone-{zoneName.Replace(".", "-")}", new PublicHostedZoneProps
            {
                ZoneName = zoneName
            });

            // Create "generic" certificate within the same region
            Certificate certificateRegion = new Certificate(this,
                                                            $"Certificate-{zoneName.Replace(".", "-")}",
                                                            CreateCertificateProps(props.Environment, zoneName, publicHostedZone));

            // Saving values as parameters for easy reuse by other constructs
            _ = new StringParameter(this, "PublicZone-HostedZoneId-Param", new StringParameterProps
            {
                ParameterName = CraveParameters.DNS.AppLess_Public.HostedZoneId,
                StringValue = publicHostedZone.HostedZoneId,
                Tier = ParameterTier.STANDARD
            });

            _ = new StringParameter(this, "PublicZone-HostedZoneName-Param", new StringParameterProps
            {
                ParameterName = CraveParameters.DNS.AppLess_Public.HostedZoneName,
                StringValue = zoneName,
                Tier = ParameterTier.STANDARD
            });

            _ = new StringParameter(this, "Certificate-Region-ARN-Param", new StringParameterProps
            {
                ParameterName = CraveParameters.Acm.AppLess_Public,
                StringValue = certificateRegion.CertificateArn,
                Tier = ParameterTier.STANDARD
            });
        }

        private static string GenerateZoneName(EnvironmentType environment, string fqdnPostfix) => environment switch
        {
            EnvironmentType.Development => $"dev.{fqdnPostfix}",
            EnvironmentType.Test => $"test.{fqdnPostfix}",
            EnvironmentType.Staging => $"stag.{fqdnPostfix}",
            EnvironmentType.Production => $"prod.{fqdnPostfix}",
            _ => throw new NotSupportedException($"Hosted Zone is not supported for this environment: {environment}")
        };

        private static CertificateProps CreateCertificateProps(EnvironmentType environmentType, string zoneName, IHostedZone hostedZone) => environmentType switch
        {
            EnvironmentType.Production => new CertificateProps
            {
                DomainName = zoneName,
                SubjectAlternativeNames = new[] { $"*.{zoneName}", $"{ZONE_NAME_BASE}", $"*.{ZONE_NAME_BASE}" },
                Validation = CertificateValidation.FromDns() // Workaround for cross-account domain verification. Will have to manually create validation CNAME records
            },
            _ => new CertificateProps
            {
                DomainName = zoneName,
                SubjectAlternativeNames = new[] { $"*.{zoneName}" },
                Validation = CertificateValidation.FromDns(hostedZone) // Passing the hosted zone enables automatic DNS validation
            }
        };
    }
}