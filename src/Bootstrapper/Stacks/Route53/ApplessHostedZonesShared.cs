﻿using Amazon.CDK;
using Amazon.CDK.AWS.Route53;
using Amazon.CDK.AWS.SSM;
using Constructs;
using Crave.Infrastructure;

namespace Bootstrapper.Stacks.Route53
{
    public class ApplessHostedZonesShared : Stack
    {
        public ApplessHostedZonesShared(Construct scope, string id, IStackProps props) : base(scope, id, props)
        {
            HostedZone publicHostedZone = new PublicHostedZone(this, "PublicZone-appless-app", new PublicHostedZoneProps
            {
                ZoneName = "appless.app"
            });

            CreateZoneDelegationRecord(publicHostedZone, "dev", new[] { "ns-376.awsdns-47.com.", "ns-1804.awsdns-33.co.uk.", "ns-1426.awsdns-50.org.", "ns-555.awsdns-05.net." });
            CreateZoneDelegationRecord(publicHostedZone, "test", new[] { "ns-207.awsdns-25.com.", "ns-1058.awsdns-04.org.", "ns-615.awsdns-12.net.", "ns-1835.awsdns-37.co.uk." });
            CreateZoneDelegationRecord(publicHostedZone, "stag", new[] { "ns-893.awsdns-47.net.", "ns-466.awsdns-58.com.", "ns-1709.awsdns-21.co.uk.", "ns-1413.awsdns-48.org." });
            CreateZoneDelegationRecord(publicHostedZone, "prod", new[] { "ns-1175.awsdns-18.org.", "ns-902.awsdns-48.net.", "ns-114.awsdns-14.com.", "ns-1636.awsdns-12.co.uk." });

            _ = new StringParameter(this, "PublicZone-HostedZoneId-Param", new StringParameterProps
            {
                ParameterName = CraveParameters.DNS.AppLess_Public.HostedZoneId,
                StringValue = publicHostedZone.HostedZoneId,
                Tier = ParameterTier.STANDARD
            });

            _ = new StringParameter(this, "PublicZone-HostedZoneName-Param", new StringParameterProps
            {
                ParameterName = CraveParameters.DNS.AppLess_Public.HostedZoneName,
                StringValue = publicHostedZone.ZoneName,
                Tier = ParameterTier.STANDARD
            });
        }

        private void CreateZoneDelegationRecord(HostedZone zone, string recordName, string[] nameServers)
        {
            string recordSetId = $"ZoneDelegate-{recordName}-appless-app";

            _ = new ZoneDelegationRecord(this, recordSetId, new ZoneDelegationRecordProps
            {
                Zone = zone,
                RecordName = recordName,
                NameServers = nameServers
            });
        }
    }
}