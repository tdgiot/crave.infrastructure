﻿'use strict';
var AWS = require('aws-sdk');

module.exports.preSignUp = (event, context, callback) => {
    var email = event.request.userAttributes.email;
    var validEmails = [
        'guido.devries@crave-emenu.com',
        'danny.kort@crave-emenu.com',
        'floris.otto@crave-emenu.com',
        'joost.vanvelthoven@crave-emenu.com',
        'robin.bijdeleij@crave-emenu.com',
        'christopher.bennie@crave-emenu.com',
        'selina.ahmed@crave-emenu.com',
        'louisa.fitzgerald@crave-emenu.com',
        'robert-jan@crave-emenu.com',
        'remco.sprenkels@crave-emenu.com',
        'claire.barrett@crave-emenu.com',
        'stephen.penn@crave-emenu.com',
        'andrew.cindrey@crave-emenu.com',
        'matthew.jones@crave-emenu.com',
        'laura.moore@crave-emenu.com',
        '_test@crave-emenu.com',
        'shaykur.rahman@crave-emenu.com',
        'sjoerd.olsthoorn@crave-emenu.com',
        'guido.devries@craveinteractive.com',
        'danny.kort@craveinteractive.com',
        'floris.otto@craveinteractive.com',
        'joost.vanvelthoven@craveinteractive.com',
        'robin.bijdeleij@craveinteractive.com',
        'christopher.bennie@craveinteractive.com',
        'selina.ahmed@craveinteractive.com',
        'louisa.fitzgerald@craveinteractive.com',
        'robert-jan@craveinteractive.com',
        'remco.sprenkels@craveinteractive.com',
        'claire.barrett@craveinteractive.com',
        'stephen.penn@craveinteractive.com',
        'andrew.cindrey@craveinteractive.com',
        'matthew.jones@craveinteractive.com',
        'laura.moore@craveinteractive.com',
        '_test@craveinteractive.com',
        'shaykur.rahman@craveinteractive.com',
        'sjoerd.olsthoorn@craveinteractive.com',
        'test_user@craveinteractive.com'
    ];

    if (validEmails.includes(email)) {
        callback(null, event);
    } else {
        var error = new Error(
            'Cannot authenticate users from this user pool app client'
        );
        callback(error, event);
    }
};
