﻿namespace Crave.Infrastructure
{
    public class CraveParameters
    {
        public static Dns DNS => new Dns();
        public class Dns
        {
            public CraveCloud CraveCloud_Public => new CraveCloud("public");
            public CraveCloud CraveCloud_Private => new CraveCloud("private");

            public class CraveCloud
            {
                public CraveCloud(string type)
                {
                    this.HostedZoneId = $"/crave/landing-zone/dns/{type}/hosted-zone-id";

                    if (type.Equals("public"))
                    {
                        this.HostedZoneName = $"/crave/landing-zone/dns/hostedzone";
                    }
                    else
                    {
                        this.HostedZoneName = $"/crave/landing-zone/dns/hostedzone-private";
                    }
                }

                public string HostedZoneId { get; }
                public string HostedZoneName { get; }
            }

            public AppLess AppLess_Public => new AppLess("public");

            public class AppLess
            {
                public AppLess(string type)
                {
                    HostedZoneId = $"/crave/infra/dns/appless/{type}/hosted-zone-id";
                    HostedZoneName = $"/crave/infra/dns/appless/{type}/hosted-zone-name";
                }

                public string HostedZoneId { get; }
                public string HostedZoneName { get; }
            }
        }

        public static Certificates Acm => new Certificates();
        public class Certificates
        {
            public string CraveCloud_Public = "/crave/infra/acm/cravecloud-public";
            public string CraveCloud_Global = "/crave/infra/acm/cravecloud-global";
            public string AppLess_Public = "/crave/infra/acm/appless-public";
        }

        public static UserPoolParams UserPool => new UserPoolParams();
        public class UserPoolParams
        {
            public string UserPoolId = "/crave/infra/userpool/user-pool-id";
            public string IdentityPoolId = "/crave/infra/userpool/identity-pool-id";
            public string AppClientId = "/crave/infra/userpool/app-client-id";
        }

        public static Services Microservices => new Services();

        public class Services
        {
            public static Service Adyen = new Service("adyen");
            public static Service Payment = new Service("payment");

            public class Service
            {
                public Service(string name)
                {
                    Private_Url = $"/crave/infra/services/{name}/private/url";
                    Function_Arn = $"/crave/infra/services/{name}/function/arn";
                }

                public string Private_Url { get; }
                public string Function_Arn { get; }
            }
        }
    }
}