﻿namespace Crave.Infrastructure
{
    public class CraveManagedPolicy
    {
        /// <summary>
        /// Policy to allow creating and writing to any /crave/* log groups
        /// </summary>
        public static string CLOUDWATCH_LOG_WRITE = "crave-cloudwatch-log-writing";

        /// <summary>
        /// Policy to allow the grafana user to read cloudwatch
        /// </summary>
        public static string GRAFANA_DASHBOARD_POLICY = "crave-grafana-dashboard-policy";
    }
}