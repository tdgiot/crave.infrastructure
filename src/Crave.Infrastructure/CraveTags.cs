﻿namespace Crave.Infrastructure
{
    public static class CraveTags
    {
        /// <summary>
        /// Tag name for setting the name of the project
        /// </summary>
        public static readonly string ProjectId = "crave:project-id";

        /// <summary>
        /// Tag name for setting the environment in which this project will be deployed
        /// </summary>
        public static readonly string EnvironmentType = "crave:environment-type";

        /// <summary>
        /// Tag name for setting the owner of the project
        /// </summary>
        public static readonly string Owner = "crave:owner";
    }
}