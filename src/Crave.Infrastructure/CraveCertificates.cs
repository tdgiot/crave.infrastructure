﻿using Crave.Infrastructure.Enums;

namespace Crave.Infrastructure
{
    public static class CraveCertificates
    {
        public static readonly string DevAwsCraveCloudXyzArn = "arn:aws:acm:ap-southeast-1:888310962618:certificate/8270b3e7-118b-4b9a-885c-9d425c724c05";
        public static readonly string ProdAwsCraveCloudXyzArn = "arn:aws:acm:ap-southeast-1:957352682348:certificate/165ff06e-ac85-41cc-bfe2-b1d40b7637dc";

        public static readonly string DevAwsCraveCloudXyzGlobalArn = "arn:aws:acm:us-east-1:888310962618:certificate/77c48018-1088-4c87-9700-4e36006472d6";
        public static readonly string ProdAwsCraveCloudXyzGlobalArn = "arn:aws:acm:us-east-1:957352682348:certificate/08170465-99b2-42ff-b68b-795df45058ec";

        public static readonly string DevAwsAppLessAppArn = "arn:aws:acm:ap-southeast-1:888310962618:certificate/8270b3e7-118b-4b9a-885c-9d425c724c05";
        public static readonly string ProdAwsAppLessAppArn = "arn:aws:acm:ap-southeast-1:957352682348:certificate/165ff06e-ac85-41cc-bfe2-b1d40b7637dc";

        /// <summary>
        /// Get the SSL Certificate ARN for a specific environment.
        /// </summary>
        /// <param name="environmentType">The environment for which to get the certificate</param>
        /// <param name="isGlobal">Specify true when using the certificate for global services (eg, CloudFront)</param>
        /// <returns></returns>
        public static string GetCraveCloudXyzCertificateArn(EnvironmentType environmentType, bool isGlobal = false)
        {
            if (isGlobal)
            {
                return environmentType switch
                {
                    EnvironmentType.Development => DevAwsCraveCloudXyzGlobalArn,
                    EnvironmentType.Production => ProdAwsCraveCloudXyzGlobalArn,
                    _ => string.Empty
                };
            }

            return environmentType switch
            {
                EnvironmentType.Development => DevAwsCraveCloudXyzArn,
                EnvironmentType.Production => ProdAwsCraveCloudXyzArn,
                _ => string.Empty
            };
        }

        /// <summary>
        /// Get the SSL Certificate ARN for a specific AppLess environment.
        /// </summary>
        /// <param name="environmentType">The environment for which to get the certificate</param>        
        /// <returns></returns>
        public static string GetAppLessAppCertificateArn(EnvironmentType environmentType)
        {
            return environmentType switch
            {
                EnvironmentType.Development => DevAwsAppLessAppArn,
                EnvironmentType.Production => ProdAwsAppLessAppArn,
                _ => string.Empty
            };
        }
    }
}