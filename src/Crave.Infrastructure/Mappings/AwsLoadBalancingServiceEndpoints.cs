﻿using System.Collections.Generic;

namespace Crave.Infrastructure.Mappings
{
    /// <summary>
    /// https://docs.aws.amazon.com/general/latest/gr/elb.html
    /// </summary>
    public class AwsLoadBalancingServiceEndpoints
    {
        public class ServiceEndpoint
        {
            internal ServiceEndpoint(string endpoint, string hostedZoneALB, string hostedZoneNLB)
            {
                this.Endpoint = endpoint;
                this.HostedZoneIdApplicationLoadBalancer = hostedZoneALB;
                this.HostedZoneIdNetworkLoadBalancer = hostedZoneNLB;
            }

            /// <summary>
            /// Elastic Load Balancing endpoint
            /// </summary>
            public string Endpoint { get; }

            /// <summary>
            /// Route 53 Hosted Zone ID (Application Load Balancers, Classic Load Balancers)
            /// </summary>
            public string HostedZoneIdApplicationLoadBalancer { get; }

            /// <summary>
            /// Route 53 Hosted Zone ID (Network Load Balancers)
            /// </summary>
            public string HostedZoneIdNetworkLoadBalancer { get; }
        }

        public static ServiceEndpoint GetServiceEndpoint(string region) => regionMapping[region];

        private static readonly Dictionary<string, ServiceEndpoint> regionMapping =
            new Dictionary<string, ServiceEndpoint>
            {
                {
                    "ap-southeast-1",
                    new ServiceEndpoint("elasticloadbalancing.ap-southeast-1.amazonaws.com", "Z1LMS91P8CMLE5",
                        "ZKVM4W9LS7TM")
                },
                {
                    "eu-west-1",
                    new ServiceEndpoint("elasticloadbalancing.eu-west-1.amazonaws.com", "Z32O12XQLNTSW2",
                        "Z2IFOLAFXWLO4F")
                },
                {
                    "eu-west-2",
                    new ServiceEndpoint("elasticloadbalancing.eu-west-2.amazonaws.com", "ZHURV8PSTC4K8",
                        "ZD4D7Y8KGAS4G")
                },
                {
                    "eu-west-3",
                    new ServiceEndpoint("elasticloadbalancing.eu-west-3.amazonaws.com", "Z3Q77PNBQS71R4",
                        "Z1CMS0P5QUZ6D5")
                },
            };
    }
}