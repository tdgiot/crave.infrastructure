﻿namespace Crave.Infrastructure.Enums
{
    public enum EnvironmentType
    {
        Playground,
        Development,
        Test,
        Staging,
        Production,
        Global,
        LocalStack
    }
}