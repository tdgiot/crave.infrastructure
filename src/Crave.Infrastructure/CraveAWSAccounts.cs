﻿namespace Crave.Infrastructure
{
    public class CraveAWSAccounts
    {
        public static readonly string Crave_Audit = "357340931525";
        public static readonly string Crave_Backup = "032139624144";
        public static readonly string Crave_Build = "001654584752";
        public static readonly string Crave_IAM = "702909098250";
        public static readonly string Crave_Logging = "566674820727";
        public static readonly string Crave_SharedServices = "489156676322";

        public static readonly string CraveCloud_Development = "243208471992";
        public static readonly string CraveCloud_Test = "765825458397";
        public static readonly string CraveCloud_Staging = "638352662757";
        public static readonly string CraveCloud_Production = "939904925660";
    }
}
