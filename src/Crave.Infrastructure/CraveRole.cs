﻿namespace Crave.Infrastructure
{
    public class CraveRole
    {
        public static string DEFAULT_BEANSTALK_WEB_TIER_ROLE = "crave-default-beanstalk-web-tier-role";
        public static string DEFAULT_BEANSTALK_WORKER_ROLE = "crave-default-beanstalk-worker-role";
        public static string DEFAULT_BEANSTALK_SERVICE_ROLE = "crave-default-beanstalk-service-role";
        public static string CRAVE_GRAFANA_ROLE = "crave-service-grafana";
    }
}
