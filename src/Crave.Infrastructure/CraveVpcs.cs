﻿using Crave.Infrastructure.Models;

namespace Crave.Infrastructure
{
    public static class CraveVpcs
    {
        public static readonly CraveVpc CraveVpc = new CraveVpc("crave-vpc");
    }
}