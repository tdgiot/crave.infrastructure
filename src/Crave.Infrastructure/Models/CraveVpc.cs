﻿namespace Crave.Infrastructure.Models
{
    public class CraveVpc
    {
        internal CraveVpc(string vpcName)
        {
            this.VpcName = vpcName;
        }

        public string VpcName { get; }
    }
}