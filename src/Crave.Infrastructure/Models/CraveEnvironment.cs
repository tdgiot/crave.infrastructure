﻿using Crave.Infrastructure.Enums;

namespace Crave.Infrastructure.Models
{
    public class CraveEnvironment
    {
        internal CraveEnvironment(string accountId,
            string region,
            EnvironmentType environment,
            DomainName domainNames)
        {
            this.AccountId = accountId;
            this.Region = region;
            this.Environment = environment;
            this.DomainNames = domainNames;
        }

        public string AccountId { get; }
        public string Region { get; }
        public EnvironmentType Environment { get; }
        public DomainName DomainNames { get; }

        public class DomainName
        {
            public string External { get; internal set; }
            public string Internal { get; internal set; }
            public string AppLess { get; internal set; }
        }
    }
}