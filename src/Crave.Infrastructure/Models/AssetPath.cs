﻿namespace Crave.Infrastructure.Models
{
    public readonly struct AssetPath
    {
        private string Value { get; }

        private AssetPath(string value) => Value = value;

        public static implicit operator string(AssetPath assetPath) => assetPath.Value;

        public static implicit operator AssetPath(string value) => new(value);
    }
}
