﻿using System;
using Crave.Infrastructure.Enums;
using Crave.Infrastructure.Models;

namespace Crave.Infrastructure
{
    public static class CraveEnvironments
    {
        public static CraveEnvironment GetDeploymentEnvironment()
        {
            string? deploymentEnvironment = Environment.GetEnvironmentVariable("DEPLOY_ENV");

            if (deploymentEnvironment == null)
            {
                throw new ArgumentNullException(nameof(deploymentEnvironment), "Environment variable 'DEPLOY_ENV' is not set");
            }

            return GetDeploymentEnvironment(deploymentEnvironment);
        }

        public static CraveEnvironment GetDeploymentEnvironment(EnvironmentType environmentType)
        {
            return environmentType switch
            {
                EnvironmentType.Development => CraveCloudDevelopment,
                EnvironmentType.Production => CraveCloudProduction,
                EnvironmentType.Global => CraveSharedServices,
                EnvironmentType.LocalStack => LocalStack,
                _ => throw new ArgumentOutOfRangeException(nameof(environmentType), environmentType, null)
            };
        }

        public static CraveEnvironment GetDeploymentEnvironment(string? deploymentEnvironment)
        {
            if (deploymentEnvironment == null)
            {
                throw new ArgumentNullException(nameof(deploymentEnvironment), "No deployment environment specified");
            }

            return deploymentEnvironment switch
            {
                "dev" => CraveCloudDevelopment,
                "production" => CraveCloudProduction,
                "local" => LocalStack,
                _ => throw new ArgumentOutOfRangeException(nameof(deploymentEnvironment), deploymentEnvironment, "The following environments are supported: playground, dev, test, staging, production, local")
            };
        }

        public static readonly CraveEnvironment LocalStack = new CraveEnvironment("000000000000", "ap-southeast-1", EnvironmentType.LocalStack,
              new CraveEnvironment.DomainName
              {
                  Internal = "localstack.trueguest.internal",
                  External = "localstack.trueguest.tech",
                  AppLess = string.Empty
              });

        public static readonly CraveEnvironment CraveCloudDevelopment = new CraveEnvironment("888310962618", "ap-southeast-1", EnvironmentType.Development,
            new CraveEnvironment.DomainName
            {
                Internal = "dev.trueguest.internal",
                External = "dev.trueguest.tech",
                AppLess = "dev.trueguest.tech"
            });

        public static readonly CraveEnvironment CraveCloudProduction = new CraveEnvironment("957352682348", "ap-southeast-1", EnvironmentType.Production,
            new CraveEnvironment.DomainName
            {
                Internal = "prod.trueguest.internal",
                External = "prod.trueguest.tech",
                AppLess = "prod.trueguest.tech"
            });

        public static readonly CraveEnvironment CraveSharedServices = new CraveEnvironment("431193470299", "ap-southeast-1", EnvironmentType.Global,
            new CraveEnvironment.DomainName
            {
                Internal = string.Empty,
                External = "trueguest.tech",
                AppLess = "trueguest.tech"
            });
    }
}
