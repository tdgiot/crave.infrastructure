﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Amazon.CDK;
using Amazon.CDK.AWS.EC2;
using Amazon.CDK.AWS.ElasticBeanstalk;
using Amazon.CDK.AWS.IAM;
using Amazon.CDK.AWS.S3.Assets;
using Constructs;

namespace Crave.Infrastructure.CDK.Constructs
{
    public class ElasticBeanstalk : Construct
    {
        public enum ElasticBeanstalkType
        {
            WebTier,
            Worker
        }

        /// <summary>
        /// The endpoint of this ElasticBeanstalk environment
        /// </summary>
        public string EnvironmentEndpointUrlAttr { get; }

        public class ElasticBeanstalkProps
        {
            /// <summary>
            /// Name of the Elastic Beanstalk application
            /// </summary>
            public string ApplicationName { get; set; } = string.Empty;

            /// <summary>
            /// A comma-separated list of instance types that you want your environment to use. For example: t2.micro,t3.micro
            /// </summary>
            /// <remarks>
            /// Instances types available: https://aws.amazon.com/ec2/instance-types/
            /// </remarks>
            public string InstanceTypes { get; set; } = "t3.micro";

            /// <summary>
            /// Minimum number of instances to spawn (min: 1)
            /// </summary>
            public int AutoScalingMinSize { get; set; } = 1;

            /// <summary>
            /// Maximum number of instances to scale out to (max: 10)
            /// </summary>
            public int AutoScalingMaxSize { get; set; } = 1;

            /// <summary>
            /// ARN of the SSL certificate to use
            /// </summary>
            /// <remarks>
            /// See <see cref="CraveCertificates"/> for available certificates
            /// </remarks>
            public string? SslCertificateArn { get; set; }

            /// <summary>
            /// If specified, the environment attempts to use this value as the prefix for the CNAME in your Elastic Beanstalk environment URL. If not specified, the CNAME is generated automatically by appending a random alphanumeric string to the environment name
            /// </summary>
            /// <remarks>
            /// <strong>Link</strong>: http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-beanstalk-environment.html#cfn-beanstalk-environment-cnameprefix
            /// </remarks>
            public string? CnamePrefix { get; set; }

            /// <summary>
            /// The platform that will be running the Beanstalk application (Default is .NET Core on Linux) 
            /// </summary>
            /// <remarks>
            /// <strong>Link</strong>: https://docs.aws.amazon.com/elasticbeanstalk/latest/platforms/platforms-supported.html#platforms-supported.dotnetlinux
            /// </remarks>
            public string SolutionStackName { get; set; } = string.Empty;

            /// <summary>
            /// The endpoint used by the load-balancer for checking the health of underlying EC2 machines (default: /health)
            /// </summary>
            public string HealthEndpoint { get; set; } = "/health";

            /// <summary>
            /// Path of the folder where to be deployed artifacts are located, based on the current working directory
            /// </summary>
            public string PublishedArtifactsFolder { get; set; } = string.Empty;

            /// <summary>
            /// When true, the load balancer will be put in a private subnet and not directly accessible from the internet
            /// </summary>
            public bool LoadBalancerPrivateSubnet { get; set; } = false;

            /// <summary>
            /// The type of ElasticBeanstalk application. (Default is WebTier)
            /// </summary>
            public ElasticBeanstalkType Type { get; set; } = ElasticBeanstalkType.WebTier;

            /// <summary>
            /// When true the application will be spawned without a load-balancer and auto-scaling will be disabled
            /// </summary>
            /// <remarks>
            /// THIS CAN NOT BE CHANGED AFTERWARDS. Only useful when you are 100% sure you only need 1 instance, ever.
            /// </remarks>
            public bool SingleInstance { get; set; } = false;

            /// <summary>
            /// Any additional/custom environment options
            /// </summary>
            /// <remarks>
            /// <strong>Link</strong>: https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/command-options-general.html
            /// </remarks>
            public List<CfnEnvironment.OptionSettingProperty>? AdditionalConfigurationOptions { get; set; } = null;

            /// <summary>
            /// Custom role to be attached to the EC2 instance profile.
            /// If you choose to use this option, good luck, make sure include all the correct policies.
            /// </summary>
            public Role? CustomInstanceProfileRole { get; set; }

            /// <summary>
            /// Enable or disable XRay tracing - Default is enabled
            /// </summary>
            public bool XRayEnabled { get; set; } = true;
        }

        public static void ValidateStackProps(string id, ElasticBeanstalkProps props)
        {
            if (props.SingleInstance)
            {
                props.AutoScalingMinSize = 1;
                props.AutoScalingMaxSize = 1;
            }

            if (props.ApplicationName.Length == 0)
            {
                throw new ArgumentException($"Stack {id}: Property 'ApplicationName' can not be empty");
            }

            if (props.InstanceTypes.Length == 0)
            {
                throw new ArgumentException($"Stack {id}: Property 'InstanceTypes' can not be empty");
            }

            if (props.PublishedArtifactsFolder.Length == 0)
            {
                throw new ArgumentException($"Stack {id}: Property 'PublishedArtifactsFolder' can not be empty");
            }

            if (props.AutoScalingMinSize > props.AutoScalingMaxSize)
            {
                throw new ArgumentException($"Stack {id}: AutoScalingMinSize can not be larger than AutoScalingMaxSize");
            }

            if (props.AutoScalingMinSize <= 0)
            {
                props.AutoScalingMinSize = 1;
            }

            if (props.AutoScalingMaxSize <= 0)
            {
                props.AutoScalingMaxSize = 1;
            }

            if (props.AutoScalingMaxSize > 10)
            {
                props.AutoScalingMaxSize = 10;
            }
            // https://docs.aws.amazon.com/elasticbeanstalk/latest/platforms/platforms-supported.html#platforms-supported.dotnetlinux
            if (props.SolutionStackName.Length == 0)
            {
                props.SolutionStackName = "64bit Amazon Linux 2 v2.2.7 running .NET Core";
            }

            if (props.HealthEndpoint.Length == 0)
            {
                props.HealthEndpoint = "/health";
            }
        }

        public ElasticBeanstalk(Construct scope, string id, ElasticBeanstalkProps props) : base(scope, id)
        {
            ValidateStackProps(id, props);

            // Role to be assigned to the EC2 machines for running the application
            IRole ebRole;
            if (props.CustomInstanceProfileRole != null)
            {
                ebRole = props.CustomInstanceProfileRole;
            }
            else
            {
                string role = props.Type == ElasticBeanstalkType.WebTier ? CraveRole.DEFAULT_BEANSTALK_WEB_TIER_ROLE : CraveRole.DEFAULT_BEANSTALK_WORKER_ROLE;
                ebRole = Role.FromRoleArn(this, "imported-eb-role", $"arn:aws:iam::{Stack.Of(this).Account}:role/{role}");
            }

            // Role used by the EB service for health reporting and maintenance
            IRole ebServiceRole = Role.FromRoleArn(this, "imported-eb-service-role", $"arn:aws:iam::{Stack.Of(this).Account}:role/{CraveRole.DEFAULT_BEANSTALK_SERVICE_ROLE}");

            // Create instance profile which will be linked to the EC2 machines. Is needed to actually let the machine start.
            CfnInstanceProfile ebInstanceProfile = new(this, "BeanStalkInstanceProfile", new CfnInstanceProfileProps
            {
                InstanceProfileName = $"{props.ApplicationName.Replace(" ", "-")}-instance-profile",
                Roles = new string[] { ebRole.RoleName }
            });

            // Start configuration of Elastic Beanstalk application and environment
            CfnApplication app = new(this, "Application", new CfnApplicationProps
            {
                ApplicationName = props.ApplicationName
            });

            // Lookup our main VPC
            IVpc craveVpc = Vpc.FromLookup(this, "crave-vpc", new VpcLookupOptions
            {
                VpcName = CraveVpcs.CraveVpc.VpcName
            });

            string publicSubnets = string.Join(",", craveVpc.PublicSubnets.Select(x => x.SubnetId));
            string privateSubnets = string.Join(",", craveVpc.PrivateSubnets.Select(x => x.SubnetId));

            // Options usable for configuration can be found on:
            // https://docs.aws.amazon.com/elasticbeanstalk/latest/dg/command-options-general.html
            List<CfnEnvironment.OptionSettingProperty> options = new()
            {
                CreateSettingProperty("aws:autoscaling:launchconfiguration", "IamInstanceProfile", ebInstanceProfile.AttrArn),

                CreateSettingProperty("aws:ec2:instances", "InstanceTypes", props.InstanceTypes),
                CreateSettingProperty("aws:ec2:vpc", "VPCId", craveVpc.VpcId),

                CreateSettingProperty("aws:ec2:vpc", "Subnets", privateSubnets),

                CreateSettingProperty("aws:elasticbeanstalk:environment", "ServiceRole", ebServiceRole.RoleArn),

                CreateSettingProperty("aws:elasticbeanstalk:environment:process:default", "HealthCheckPath", props.HealthEndpoint),
                CreateSettingProperty("aws:elasticbeanstalk:healthreporting:system", "ConfigDocument", "{\"Rules\": { \"Environment\": { \"Application\": { \"ApplicationRequests4xx\": { \"Enabled\": false } }, \"ELB\": { \"ELBRequests4xx\": {\"Enabled\": false } } } }, \"Version\": 1 }"),
                CreateSettingProperty("aws:elasticbeanstalk:healthreporting:system", "SystemType", "enhanced"),
                CreateSettingProperty("aws:elasticbeanstalk:xray", "XRayEnabled", props.XRayEnabled.ToString().ToLower()),

                CreateSettingProperty("aws:elasticbeanstalk:managedactions", "ManagedActionsEnabled", "true"),
                CreateSettingProperty("aws:elasticbeanstalk:managedactions", "PreferredStartTime", "MON:04:00"),
                CreateSettingProperty("aws:elasticbeanstalk:managedactions:platformupdate", "UpdateLevel", "minor")
            };

            if (props.SingleInstance)
            {
                // Single instance, no load balancing and auto-scaling
                options.Add(CreateSettingProperty("aws:elasticbeanstalk:environment", "EnvironmentType", "SingleInstance"));
                options.Add(CreateSettingProperty("aws:autoscaling:asg", "MaxSize", 1.ToString()));
            }
            else
            {
                options.Add(CreateSettingProperty("aws:autoscaling:asg", "MinSize", props.AutoScalingMinSize.ToString()));
                options.Add(CreateSettingProperty("aws:autoscaling:asg", "MaxSize", props.AutoScalingMaxSize.ToString()));

                options.Add(CreateSettingProperty("aws:elasticbeanstalk:environment", "EnvironmentType", "LoadBalanced"));
                options.Add(CreateSettingProperty("aws:elasticbeanstalk:environment", "LoadBalancerType", "application"));
                options.Add(CreateSettingProperty("aws:ec2:vpc", "ELBSubnets", props.LoadBalancerPrivateSubnet ? privateSubnets : publicSubnets));
                options.Add(CreateSettingProperty("aws:ec2:vpc", "ELBScheme", props.LoadBalancerPrivateSubnet ? "internal" : "public"));
                options.Add(CreateSettingProperty("aws:ec2:vpc", "AssociatePublicIpAddress", "false")); // only true when both LB & EC2 are in public subnet

                // Set rolling and batch update parameters when running multiple instances
                if (props.AutoScalingMaxSize > 1)
                {
                    options.Add(CreateSettingProperty("aws:autoscaling:updatepolicy:rollingupdate", "RollingUpdateEnabled", "true"));
                    options.Add(CreateSettingProperty("aws:autoscaling:updatepolicy:rollingupdate", "RollingUpdateType", "Health"));
                    options.Add(CreateSettingProperty("aws:autoscaling:updatepolicy:rollingupdate", "MaxBatchSize", "1"));
                    options.Add(CreateSettingProperty("aws:autoscaling:updatepolicy:rollingupdate", "MinInstancesInService", "1"));

                    options.Add(CreateSettingProperty("aws:elasticbeanstalk:command", "DeploymentPolicy", "Rolling"));
                    options.Add(CreateSettingProperty("aws:elasticbeanstalk:command", "BatchSizeType", "Fixed"));
                    options.Add(CreateSettingProperty("aws:elasticbeanstalk:command", "BatchSize", "1"));
                }
            }

            // When SSL certificate has been supplied, disable listener on port 80 and
            // create new HTTPS listener on 443 linked to certificate
            if (!string.IsNullOrWhiteSpace(props.SslCertificateArn))
            {
                options.Add(CreateSettingProperty("aws:elbv2:listener:default", "ListenerEnabled", "false"));
                options.Add(CreateSettingProperty("aws:elbv2:listener:443", "Protocol", "HTTPS"));
                options.Add(CreateSettingProperty("aws:elbv2:listener:443", "SSLCertificateArns", props.SslCertificateArn));
            }

            if (props.AdditionalConfigurationOptions != null)
            {
                // Add additional options from properties
                options.AddRange(props.AdditionalConfigurationOptions);
            }

            // Pack published application assets. This will zip and upload them to S3
            Asset asset = new(this, "AppAsset", new AssetProps
            {
                Path = Path.Combine(Directory.GetCurrentDirectory(), props.PublishedArtifactsFolder)
            });

            // Create a new application version based on the above created asset
            CfnApplicationVersion applicationVersion = new(this, "AppVersion", new CfnApplicationVersionProps
            {
                ApplicationName = props.ApplicationName,
                SourceBundle = new CfnApplicationVersion.SourceBundleProperty
                {
                    S3Bucket = asset.S3BucketName,
                    S3Key = asset.S3ObjectKey
                }
            });

            // Create the actual Elastic Beanstalk environment
            // This will auto-create things like auto-scaling groups, load-balancer and EC2 machines
            // https://docs.aws.amazon.com/elasticbeanstalk/latest/platforms/platforms-supported.html#platforms-supported.dotnetlinux
            CfnEnvironment environment = CreateEnvironment(props, options, applicationVersion);

            // Make sure stuff is created in the correct order
            applicationVersion.AddDependsOn(app);
            environment.AddDependsOn(ebInstanceProfile);
            environment.AddDependsOn(app);

            this.EnvironmentEndpointUrlAttr = environment.AttrEndpointUrl;
        }

        public static CfnEnvironment.OptionSettingProperty CreateSettingProperty(string optionNamespace, string optionName, string value)
        {
            return new()
            {
                Namespace = optionNamespace,
                OptionName = optionName,
                Value = value
            };
        }

        private CfnEnvironment CreateEnvironment(ElasticBeanstalkProps props, List<CfnEnvironment.OptionSettingProperty> options, CfnApplicationVersion applicationVersion)
        {
            CfnEnvironment environment = new(this, "Environment", new CfnEnvironmentProps
            {
                EnvironmentName = $"{props.ApplicationName}",
                ApplicationName = props.ApplicationName,
                SolutionStackName = props.SolutionStackName,
                OptionSettings = options.ToArray(),
                VersionLabel = applicationVersion.Ref,
                CnamePrefix = props.CnamePrefix
            });

            if (props.Type == ElasticBeanstalkType.Worker)
            {
                environment.Tier = new CfnEnvironment.TierProperty
                {
                    Name = "Worker",
                    Type = "SQS/HTTP"
                };
            }
            return environment;
        }
    }
}
