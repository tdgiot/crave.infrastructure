﻿using Amazon.CDK;
using Amazon.CDK.AWS.Route53;
using Crave.Infrastructure.CDK.Constructs;
using Crave.Infrastructure.Mappings;

namespace Crave.Infrastructure.CDK.Resources
{
    public class ElasticBeanstalkARecord
    {
        public ElasticBeanstalkARecord(ElasticBeanstalk eb, string recordName, string domainName)
        {
            ARecord record = new ARecord(eb, "hosted-zone-a-record", new ARecordProps
            {
                RecordName = recordName,
                Target = RecordTarget.FromAlias(new BeanstalkTarget(new AliasRecordTargetConfig
                {
                    DnsName = eb.EnvironmentEndpointUrlAttr,
                    HostedZoneId = AwsLoadBalancingServiceEndpoints.GetServiceEndpoint(Stack.Of(eb).Region).HostedZoneIdApplicationLoadBalancer,
                })),
                Zone = HostedZone.FromLookup(eb, "hosted-zone", new HostedZoneProviderProps
                {
                    DomainName = domainName
                })
            });

            record.Node.AddDependency(eb);
        }
    }
}
