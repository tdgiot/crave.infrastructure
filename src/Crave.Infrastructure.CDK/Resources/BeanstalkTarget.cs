﻿using Amazon.CDK.AWS.Route53;
using Amazon.JSII.Runtime.Deputy;

namespace Crave.Infrastructure.CDK.Resources
{
    public class BeanstalkTarget : DeputyBase, IAliasRecordTarget
    {
        private readonly IAliasRecordTargetConfig aliasRecordTargetConfig;

        public BeanstalkTarget(IAliasRecordTargetConfig aliasRecordTargetConfig) : base(new DeputyProps(new object[] { }))
        {
            this.aliasRecordTargetConfig = aliasRecordTargetConfig;
        }

        public IAliasRecordTargetConfig Bind(IRecordSet record, IHostedZone? hostedZone)
        {
            return this.aliasRecordTargetConfig;
        }
    }
}
