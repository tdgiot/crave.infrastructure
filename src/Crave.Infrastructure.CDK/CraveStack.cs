﻿using Amazon.CDK;
using Crave.Infrastructure.CDK.Extensions;
using Crave.Infrastructure.Models;

namespace Crave.Infrastructure.CDK
{
    public class CraveStack : Stack
    {
        public CraveStack(App scope, string id, StackProps props) : base(scope, id, props)
        {
            CraveEnvironment = scope.GetCraveEnvironment();
        }

        public CraveEnvironment CraveEnvironment { get; }
    }
}
