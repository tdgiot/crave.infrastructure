﻿using Amazon.CDK;
using Crave.Infrastructure.Models;

namespace Crave.Infrastructure.CDK.Extensions
{
    public static class CraveEnvironmentExtensions
    {
        public static Environment ToCdkEnvironment(this CraveEnvironment craveEnvironment) => new Environment
        {
            Region = craveEnvironment.Region,
            Account = craveEnvironment.AccountId
        };
    }
}