﻿using Crave.Infrastructure.CDK.Constructs;
using Crave.Infrastructure.CDK.Resources;

namespace Crave.Infrastructure.CDK.Extensions
{
    public static class ElasticBeanstalkExtensions
    {
        public static ElasticBeanstalk WithARecord(this ElasticBeanstalk eb, string recordName, string domainName)
        {
            _ = new ElasticBeanstalkARecord(eb, recordName, domainName);
            return eb;
        }
    }
}
