﻿using Amazon.CDK;
using Amazon.CDK.AWS.APIGateway;
using Constructs;
using Crave.Infrastructure.CDK.Helpers;
using static Crave.Infrastructure.CDK.Helpers.ApiGatewayHelper;
using Stage = Amazon.CDK.AWS.APIGateway.Stage;

namespace Crave.Infrastructure.CDK.Extensions
{
    public static class RestApiExtensions
    {
        /// <summary>
        /// Add a base path mapping to the gateway.*.aws.cravecloud.xyz custom domain. Construct will be created with RemovalPolicy.DESTROY
        /// </summary>
        /// <param name="scope">The CDK scope</param>
        /// <param name="id">Id for the construct</param>
        /// <param name="restApi">The RestAPI to link to the mapping</param>
        /// <param name="basePath">The base path name that callers of the API must provide in the URL after the domain name (e.g. gateway.dev.aws.cravecloud.xyz/base-path). It can't be an empty string.</param>
        /// <param name="stage">The Deployment stage of API - default ("prod") will be used when null</param>
        /// <returns>Returns the <see cref="BasePathMapping"/> construct</returns>
        public static BasePathMapping AddBasePathMappingForCraveCloud(this IRestApi restApi, Construct scope, string baseId, string basePath, Stage? stage = null) => ApiGatewayHelper.AddBasePathMappingForCraveCloud(scope, baseId, restApi, basePath, stage);

        /// <summary>
        /// Create a method which will proxy the request to an AWS service (such as SQS)
        /// </summary>
        /// <param name="resource">The resource to create the method on </param>
        /// <param name="props">The properties for creating the proxy</param>
        /// <returns>Returns the newly created <see cref="Method"/> construct</returns>
        public static Method AddAwsServiceProxyMethod(this Amazon.CDK.AWS.APIGateway.Resource resource, AddAwsServiceProxyMethodProps props) => ApiGatewayHelper.AddAwsServiceProxyMethod(resource, props);
    }
}
