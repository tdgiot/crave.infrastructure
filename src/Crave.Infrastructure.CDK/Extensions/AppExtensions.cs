﻿using Amazon.CDK;
using Crave.Infrastructure.Models;
using System;

namespace Crave.Infrastructure.CDK.Extensions
{
    public static class AppExtensions
    {
        public static CraveEnvironment GetCraveEnvironment(this App app)
        {
            if (app.Node.TryGetContext("deploy_env", out string contextValue))
            {
                return CraveEnvironments.GetDeploymentEnvironment(contextValue);
            }

            return CraveEnvironments.GetDeploymentEnvironment();
        }

        public static AssetPath GetAssetPath(this App app)
        {
            if (!app.Node.TryGetContext("asset_path", out string assetPath))
            {
                throw new InvalidOperationException("Missing required asset_path which should be passed in as context");
            }

            return assetPath;
        }
    }
}