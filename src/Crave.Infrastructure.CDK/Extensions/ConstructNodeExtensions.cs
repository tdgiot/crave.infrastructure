﻿using Constructs;

namespace Crave.Infrastructure.CDK.Extensions
{
    public static class ConstructNodeExtensions
    {
        public static bool TryGetContext<T>(this Node node, string key, out T contextValue)
        {
            T result = (T)node.TryGetContext(key);
            if (result == null || result.Equals(default(T)))
            {
                contextValue = default!; // suppress null assignment warning
                return false;
            }

            contextValue = result;

            return true;
        }
    }
}