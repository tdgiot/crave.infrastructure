﻿using System;
using System.Collections.Generic;
using Amazon.CDK;
using Amazon.CDK.AWS.APIGateway;
using Amazon.CDK.AWS.IAM;
using Amazon.CDK.AWS.SSM;
using Constructs;
using Stage = Amazon.CDK.AWS.APIGateway.Stage;

namespace Crave.Infrastructure.CDK.Helpers
{
    public static class ApiGatewayHelper
    {
        /// <summary>
        /// Add a base path mapping to the gateway.*.aws.cravecloud.xyz custom domain. Construct will be created with RemovalPolicy.DESTROY
        /// </summary>
        /// <param name="scope">The CDK scope</param>
        /// <param name="id">Id for the construct</param>
        /// <param name="restApi">The RestAPI to link to the mapping</param>
        /// <param name="basePath">The base path name that callers of the API must provide in the URL after the domain name (e.g. gateway.dev.aws.cravecloud.xyz/base-path). It can't be an empty string.</param>
        /// <param name="stage">The Deployment stage of API - default ("prod") will be used when null</param>
        /// <returns>Returns the <see cref="BasePathMapping"/> construct</returns>
        public static BasePathMapping AddBasePathMappingForCraveCloud(Construct scope, string id, IRestApi restApi, string basePath, Stage? stage = null)
        {
            if (basePath.Length == 0)
            {
                throw new ArgumentException("Base path has to be supplied", nameof(basePath));
            }

            string domainBase = StringParameter.FromStringParameterName(scope, "hostedzone-name-param", CraveParameters.DNS.CraveCloud_Public.HostedZoneName).StringValue;
            string hostedZoneId = StringParameter.FromStringParameterName(scope, "hostedzone-id-param", CraveParameters.DNS.CraveCloud_Public.HostedZoneId).StringValue;

            // Lookup the custom domain name
            IDomainName domain = DomainName_.FromDomainNameAttributes(scope, $"api-custom-domain-lookup", new DomainNameAttributes
            {
                DomainName = $"gateway.{domainBase}",
                DomainNameAliasHostedZoneId = hostedZoneId,
                DomainNameAliasTarget = "gateway"
            });

            // Create a new mapping which will map /myservice to an API Gateway
            BasePathMapping mapping = new BasePathMapping(scope, id, new BasePathMappingProps
            {
                DomainName = domain,
                BasePath = basePath, // gateway.dev.aws.cravecloud.xyz/myservice
                RestApi = restApi,
                Stage = stage
            });
            mapping.ApplyRemovalPolicy(RemovalPolicy.DESTROY);

            return mapping;
        }

        public struct AddAwsServiceProxyMethodProps
        {
            public string ApiMethod { get; set; }
            public string Service { get; set; }
            public string Path { get; set; }
            public IRole ApiGatewayRole { get; set; }
            public string RequestTemplate { get; set; }
            public string? ContentType { get; set; }
            public MethodOptions? MethodOptions { get; set; }
        }

        public static Method AddAwsServiceProxyMethod(Amazon.CDK.AWS.APIGateway.Resource resource, AddAwsServiceProxyMethodProps props)
        {
            if (props.MethodOptions == null)
            {
                props.MethodOptions = new MethodOptions
                {
                    MethodResponses = new[]
                    {
                        new MethodResponse
                        {
                            StatusCode = "200",
                            ResponseParameters = new Dictionary<string, bool> {{ "method.response.header.Content-Type", true }}
                        },
                        new MethodResponse
                        {
                            StatusCode = "500",
                            ResponseParameters = new Dictionary<string, bool> {{ "method.response.header.Content-Type", true }}
                        }
                    }
                };
            }

            if (props.ContentType != null && !props.ContentType.StartsWith("'"))
            {
                props.ContentType = $"'{props.ContentType}'";
            }

            return resource.AddMethod(props.ApiMethod,
                new AwsIntegration(new AwsIntegrationProps
                {
                    Service = props.Service,
                    Path = props.Path,
                    IntegrationHttpMethod = "POST",
                    Options = new IntegrationOptions
                    {
                        CredentialsRole = props.ApiGatewayRole,
                        PassthroughBehavior = PassthroughBehavior.NEVER,
                        RequestParameters = new Dictionary<string, string>
                                    {
                                        { "integration.request.header.Content-Type", (props.ContentType ?? "'application.json'") }
                                    },
                        RequestTemplates = new Dictionary<string, string>
                                    {
                                        { "application/json", props.RequestTemplate }
                                    },
                        IntegrationResponses = new[]
                                    {
                                        new IntegrationResponse
                                        {
                                            StatusCode = "200"
                                        },
                                        new IntegrationResponse
                                        {
                                            StatusCode = "500",
                                            ResponseTemplates = new Dictionary<string, string> { { "text/html", "Error" } },
                                            SelectionPattern = "500"
                                        }
                                    }
                    }
                }),
                props.MethodOptions);
        }
    }
}
