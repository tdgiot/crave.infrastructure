# Crave.Infrastructure 
The `Crave.Infrastructure` project is compiled into a NuGet package to be consumed by other Crave CDK projects. The project contains constants for AWS environments, VPCs and Tagging.

Besides the NuGet package this project also contains some core infrastructure components which are applied to all environments (dev, test, staging, production) such as:
* Custom domain name for API Gateway (`gateway.*.trueguest.tech`)

## Usages

### Environments
The following command will return a `CraveEnvironment` model which contains the AWS account id, region and environment enum. The active deployment environment is based on the `DEPLOY_ENV` environment variable
```
CraveEnvironment craveDeployEnv = CraveEnvironments.GetDeploymentEnvironment();
```

Using the returned `CraveEnvironment` model you are able to create a proper AWS CDK Environment
```
Amazon.CDK.Environment env = new Amazon.CDK.Environment {
    Account = craveDeployEnv.AccountId,
    Region = craveDeployEnv.Region
};
```

### VPCs
For now, all our projects and services will share the same VPC. This means that when you have to supply a VPC within your CDK project, it has to be via lookup.

```
Amazon.CDK.AWS.EC2.Vpc.FromLookup(app, "CraveVpcLookup", new Amazon.CDK.AWS.EC2.VpcLookupOptions
{
    VpcName = CraveVpcs.CraveVpc.VpcName
});
```

### Tagging
Tagging can be used for all kinds of tracking and automation. In order to keep tags consistent throughout our platform this package contains some standard tag keys.

| Name | Key | Usage |
|----|----|----|
| ProjectId | crave:project-id | Set the name of your project or service |
| EnvironmentType | crave:environment-type | The environment the service is deployed in |

Tags can be put a whole stack on separate constructs, using the same mechanism. When you place tags on your complete stack all constructs within that stack will automatically inherit those tags.
```
Amazon.CDK.Tags.Of(stack).Add(CraveTags.ProjectId, "ordering-service");
Amazon.CDK.Tags.Of(stack).Add(CraveTags.EnvironmentType, craveDeployEnv.Environment);
```

### Certificates
A certificate has been created in each of the application accounts (dev, test, staging and production) for the `aws.cravecloud.xyz` domain. 
The certificates also contains a wildcard domain (eg, `*.dev.aws.cravecloud.xyz` so it can easily be used when an appliation needs it's own endpoint. 
This package contains the ARN ids of the certificates which can be used to fetch the certificate or link it to an other resource.

```
// Fetch by ARN directly
Certificate.FromCertificateArn(this, "My-Certificate", CraveCertificates.DevAwsCraveCloudXyzArn);

// Use helper class to give you the certificate ARN based on the environment
EnvironmentType currentEnv = EnvironmentType.Development;
Certificate.FromCertificateArn(this, "My-Certificate", CraveCertificates.GetCraveCloudXyzCertificateArn(currentEnv));
```

### API Gateway
Each environment contains a regional custom domain to which service specific API Gateways can be mapped to. The base url is: `gateway.{env}.aws.cravecloud.xyz`.
To create a mapping for your API Gateway you will first have to lookup the `DomainName` and then create a `BasePathMapping`.

```
// Lookup the custom domain name
IDomainName domain = DomainName_.FromDomainNameAttributes(this, "gateway-domain", new DomainNameAttributes
{
    DomainName = "gateway.dev.trueguest.tech"
});

// Create a new mapping which will map /myservice to an API Gateway
_ = new BasePathMapping(this, "my-api-mapping", new BasePathMappingProps
{
    DomainName = domain,
    BasePath = "myservice", // gateway.dev.trueguest.tech/myservice
    RestApi = myRestApi
});
```

## Releasing a New NuGet Package
The `Crave.Infrastructure` package is automatically build and released on the Crave NuGet feed when a version tag is created. Create a new tag on the commit you wish to create a NuGet package for, in the style of `v0.2.0` and push the tag to the server.
The CI/CD pipeline will automatically pick up this tag and create a new NuGet package with the specified version.

&nbsp;

# CDK

The `cdk.json` file tells the CDK Toolkit how to execute your app.

It uses the [.NET Core CLI](https://docs.microsoft.com/dotnet/articles/core/) to compile and execute your project.

## Useful commands

* `dotnet build src` compile this app
* `cdk deploy`       deploy this stack to your default AWS account/region
* `cdk diff`         compare deployed stack with current state
* `cdk synth`        emits the synthesized CloudFormation template

## Making it work with your own projects
In the directory which hold your `cdk.json` file, create a new file called `cdk.context.json`. If the file already exists just append the following lines.
```
{
  "assume-role-credentials:writeIamRoleName": "crave-cloudformation-stack-execution",
  "assume-role-credentials:readIamRoleName": "crave-cloudformation-stack-execution"
}
```
This tells the _assume role plugin_ which role to use when assuming into a different account.

&nbsp;

# Bootstrapping AWS Accounts
This is a recollection of actions and commands which have been executed in order to bootstrap AWS accounts.

## Prerequisites
### Install assume role plugin
This plugin makes it so you can use one account which has permission to jump to other AWS accounts. This way there is no need to have separate deployment credentials for each account.

Use the following command to install the npm package. We're using the `-g` argument to install it globally in case you need it with other projects
```
npm install -g cdk-assume-role-credential-plugin
```

### Create new credential profile
Add a new profile (called: crave-tdg-build) to the aws `.aws/credentials` file. The access key and secret can be found in the admin KeePass.

```
[crave-build]
aws_access_key_id = <copy_from_keepass>
aws_secret_access_key = <copy_from_keepass>
region=ap-southeast-1
```

### Enable new-style bootstrapping
At this time there are two schools of bootstrapping. We are using the new/modern one. To enable the modern bootstrap template you have to set the `CDK_NEW_BOOTSTRAP` environment variable.

```
Powershell:
$Env:CDK_NEW_BOOTSTRAP = 1

Commandline:
setx CDK_NEW_BOOTSTRAP 1
```

### Init CDK Project
1. Create a new `cdk-bootstrap` folder 
2. Open a command prompt within this folder and set the 'new-style' bootstrapping (see above) 
3. Initialize a new CDK project by running `cdk init --language csharp`
4. Create a new file called `cdk.context.json` and add the json found in 'Making it work with your own projects'

## Bootstrapping the Accounts
All accounts have been bootstrapped in the `ap-southeast-1` region (Singapore).

### Account: crave-tdg-build
Firstly the `crave-tdg-build` account will be bootstrapped. This account is our center account and will be used to jump to our other AWS accounts. The other AWS accounts will have this build account listed as trusted.
```
cdk bootstrap --profile crave-tdg-build --cloudformation-execution-policies arn:aws:iam::aws:policy/AdministratorAccess aws://514368322204/ap-southeast-1 --plugin cdk-assume-role-credential-plugin --context bootstrap=true
```

### Account: crave-tdg-shs
```
cdk bootstrap --profile crave-tdg-build --trust 514368322204  --cloudformation-execution-policies arn:aws:iam::aws:policy/AdministratorAccess aws://431193470299/ap-southeast-1 --plugin cdk-assume-role-credential-plugin --context bootstrap=true
```

### Account: crave-tdg-development
```
cdk bootstrap --profile crave-tdg-build --trust 514368322204  --cloudformation-execution-policies arn:aws:iam::aws:policy/AdministratorAccess aws://888310962618/ap-southeast-1 --plugin cdk-assume-role-credential-plugin --context bootstrap=true
```

### Account: crave-tdg-production
```
cdk bootstrap --profile crave-tdg-build --trust 514368322204  --cloudformation-execution-policies arn:aws:iam::aws:policy/AdministratorAccess aws://957352682348/ap-southeast-1 --plugin cdk-assume-role-credential-plugin --context bootstrap=true
```

## Deploying Infrastructure Updates
```
# Commandline
set DEPLOY_ENV=<dev|production>
# PowerShell
$Env:DEPLOY_ENV = <dev|production>

cdk deploy --profile crave-tdg-build
=======
# Crave.Infrastructure 
The `Crave.Infrastructure` project is compiled into a NuGet package to be consumed by other Crave CDK projects. The project contains constants for AWS environments, VPCs and Tagging.

Besides the NuGet package this project also contains some core infrastructure components which are applied to all environments (dev, test, staging, production) such as:
* Public CraveCloud HostedZones (`*.aws.cravecloud.xyz`)
* Private CraveCloud HostedZones (`*.aws.cravecloud.internal`)
* `crave-emenu.com` HostedZone (production only)
* Custom domain name for API Gateway (`gateway.*.aws.cravecloud.xyz`)

## Usages

### Environments
In order to deploy a stack into different AWS accounts you can supply an environment identifier using either a environment variable or the CDK context. Where the latter one is preferred.
The following values are supported: `dev`, `test`, `staging`, `production`, and `global`. Look in the `deploy.yaml` file within this repository for an example on how to add the CDK to your DevOps pipeline.

* Environment variable: `DEPLOY_ENV`
* CDK context: `--context deploy_env=production`

To get the correct environment within the CDK application certain extension methods are available in the `Crave.Infrastructure.CDK` package.
```
App app = new App();
CraveEnvironment craveEnvironment = app.GetCraveEnvironment();
```
The `GetCraveEnvironment` extension method will first check if the deploy environment is available with the CDK context. If no context is supplied the environment variable is checked.


Once you have a `CraveEnvironment` model an AWS CDK Environment can be created using the `ToCdkEnvironment` extension method.
```
Amazon.CDK.Environment env = craveEnvironment.ToCdkEnvironment();
```

### VPCs
For now, all our projects and services will share the same VPC. This means that when you have to supply a VPC within your CDK project, it has to be via lookup.

```
Amazon.CDK.AWS.EC2.Vpc.FromLookup(app, "CraveVpcLookup", new Amazon.CDK.AWS.EC2.VpcLookupOptions
{
    VpcName = CraveVpcs.CraveVpc.VpcName
});
```

### Tagging
Tagging can be used for all kinds of tracking and automation. In order to keep tags consistent throughout our platform this package contains some standard tag keys.

| Name | Key | Usage |
|----|----|----|
| ProjectId | crave:project-id | Set the name of your project or service |
| EnvironmentType | crave:environment-type | The environment the service is deployed in |

Tags can be put a whole stack on separate constructs, using the same mechanism. When you place tags on your complete stack all constructs within that stack will automatically inherit those tags.
```
Amazon.CDK.Tags.Of(stack).Add(CraveTags.ProjectId, "ordering-service");
Amazon.CDK.Tags.Of(stack).Add(CraveTags.EnvironmentType, craveDeployEnv.Environment);
```

### Certificates
A certificate has been created in each of the application accounts (dev, test, staging and production) for the `aws.cravecloud.xyz` domain. 
The certificates also contains a wildcard domain (eg, `*.dev.aws.cravecloud.xyz` so it can easily be used when an appliation needs it's own endpoint. 
This package contains the ARN ids of the certificates which can be used to fetch the certificate or link it to an other resource.

```
// Fetch by ARN directly
Certificate.FromCertificateArn(this, "My-Certificate", CraveCertificates.DevAwsCraveCloudXyzArn);

// Use helper class to give you the certificate ARN based on the environment
EnvironmentType currentEnv = EnvironmentType.Development;
Certificate.FromCertificateArn(this, "My-Certificate", CraveCertificates.GetCraveCloudXyzCertificateArn(currentEnv));
```

### API Gateway

#### Custom Domains (Base Path Mapping)
Each environment contains a regional custom domain to which service specific API Gateways can be mapped to. The base url is: `gateway.{env}.aws.cravecloud.xyz`.
To create a mapping for your API Gateway you will first have to lookup the `DomainName` and then create a `BasePathMapping`.

```
// Lookup the custom domain name
IDomainName domain = DomainName_.FromDomainNameAttributes(this, "gateway-domain", new DomainNameAttributes
{
    DomainName = "gateway.dev.aws.cravecloud.xyz"
});

// Create a new mapping which will map /myservice to an API Gateway
_ = new BasePathMapping(this, "my-api-mapping", new BasePathMappingProps
{
    DomainName = domain,
    BasePath = "myservice", // gateway.dev.aws.cravecloud.xyz/myservice
    RestApi = myRestApi
});
```

#### AWS Service Proxy Method
API Gateway is able to communicate directly with some AWS services and allows you to configure proxy methods for this. 

The following example shows how to directly publish the contents of a POST call to an SQS queue. 
The attached API Gateway Role has to have "SendMessage" permissions on the SQS Queue.

```
apiGateway.Root
    .AddResource("queue-on-sqs")
    .AddAwsServiceProxyMethod(new Crave.Infrastructure.CDK.Helpers.ApiGatewayHelper.AddAwsServiceProxyMethodProps
    {
        ApiGatewayRole = customApiGatewayRole,
        ApiMethod = "POST",
        ContentType = "application/x-www-form-urlencoded",
        Service = "sqs",
        Path = $"{Of(this).Account}/MySqsQueueName",
        RequestTemplate = "Action=SendMessage&MessageBody=$util.urlEncode(\"$input.body\")"
    });
```

## Releasing a New NuGet Package
The `Crave.Infrastructure` package is automatically build and released on the Crave NuGet feed when a version tag is created. Create a new tag on the commit you wish to create a NuGet package for, in the style of `v0.2.0` and push the tag to the server.
The CI/CD pipeline will automatically pick up this tag and create a new NuGet package with the specified version.

&nbsp;

# CDK

The `cdk.json` file tells the CDK Toolkit how to execute your app.

It uses the [.NET Core CLI](https://docs.microsoft.com/dotnet/articles/core/) to compile and execute your project.

## Useful commands

* `dotnet build src` compile this app
* `cdk deploy`       deploy this stack to your default AWS account/region
* `cdk diff`         compare deployed stack with current state
* `cdk synth`        emits the synthesized CloudFormation template

## Making it work with your own projects
In the directory which hold your `cdk.json` file, create a new file called `cdk.context.json`. If the file already exists just append the following lines.
```
{
  "assume-role-credentials:writeIamRoleName": "crave-cloudformation-stack-execution",
  "assume-role-credentials:readIamRoleName": "crave-cloudformation-stack-execution"
}
```
This tells the _assume role plugin_ which role to use when assuming into a different account.

&nbsp;

# Bootstrapping AWS Accounts
This is a recollection of actions and commands which have been executed in order to bootstrap AWS accounts.

## Prerequisites
### Install assume role plugin
This plugin makes it so you can use one account which has permission to jump to other AWS accounts. This way there is no need to have separate deployment credentials for each account.

Use the following command to install the npm package. We're using the `-g` argument to install it globally in case you need it with other projects
```
npm install -g cdk-assume-role-credential-plugin
```

### Create new credential profile
Add a new profile (called: crave-build) to the aws `.aws/credentials` file. The access key and secret can be found in the admin KeePass

```
[crave-build]
aws_access_key_id = <copy_from_keepass>
aws_secret_access_key = <copy_from_keepass>
region=eu-west-1
```

### Enable new-style bootstrapping
At this time there are two schools of bootstrapping. We are using the new/modern one. To enable the modern bootstrap template you have to set the `CDK_NEW_BOOTSTRAP` environment variable.

```
Powershell:
$Env:CDK_NEW_BOOTSTRAP = 1

Commandline:
setx CDK_NEW_BOOTSTRAP 1
```

## Bootstrapping the Accounts
All accounts have been bootstrapped in the `eu-west-1` region (Ireland).

### Account: crave-build
Firstly the `crave-build` account will be bootstrapped. This account is our center account and will be used to jump to our other AWS accounts. The other AWS accounts will have this build account listed as trusted.
```
cdk bootstrap --profile crave-build --cloudformation-execution-policies arn:aws:iam::aws:policy/AdministratorAccess aws://001654584752/eu-west-1 --plugin cdk-assume-role-credential-plugin --context bootstrap=true
```

### Account: crave-sharedservices
```
cdk bootstrap --profile crave-build --trust 001654584752  --cloudformation-execution-policies arn:aws:iam::aws:policy/AdministratorAccess aws://489156676322/eu-west-1 --plugin cdk-assume-role-credential-plugin --context bootstrap=true
```

### Account: cravecloud-development
```
cdk bootstrap --profile crave-build --trust 001654584752  --cloudformation-execution-policies arn:aws:iam::aws:policy/AdministratorAccess aws://243208471992/eu-west-1 --plugin cdk-assume-role-credential-plugin --context bootstrap=true
```

### Account: cravecloud-test
```
cdk bootstrap --profile crave-build --trust 001654584752  --cloudformation-execution-policies arn:aws:iam::aws:policy/AdministratorAccess aws://765825458397/eu-west-1 --plugin cdk-assume-role-credential-plugin --context bootstrap=true
```

### Account: cravecloud-staging
```
cdk bootstrap --profile crave-build --trust 001654584752  --cloudformation-execution-policies arn:aws:iam::aws:policy/AdministratorAccess aws://638352662757/eu-west-1 --plugin cdk-assume-role-credential-plugin --context bootstrap=true
```

### Account: cravecloud-production
```
cdk bootstrap --profile crave-build --trust 001654584752  --cloudformation-execution-policies arn:aws:iam::aws:policy/AdministratorAccess aws://939904925660/eu-west-1 --plugin cdk-assume-role-credential-plugin --context bootstrap=true
```

## Deploying Infrastructure Updates
```
# Commandline
set DEPLOY_ENV=<dev|test|staging|production>
# PowerShell
$Env:DEPLOY_ENV = <dev|test|staging|production>

cdk deploy --profile crave-build CraveInfrastructure