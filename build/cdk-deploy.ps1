param (
	[string]$deploy_env
)

$Env:DEPLOY_ENV = $deploy_env

cdk deploy '*' --verbose --ci --require-approval never